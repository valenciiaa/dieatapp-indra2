package com.sman2.dieatApp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Recipe implements Parcelable {
    public static final Creator<Recipe> CREATOR = new Creator<Recipe>() {
        @Override
        public Recipe createFromParcel(Parcel in) {
            return new Recipe(in);
        }

        @Override
        public Recipe[] newArray(int size) {
            return new Recipe[size];
        }
    };
    private String recipeId;
    private String recipeName;
    private String drinkAlternate;
    private String category;
    private String area;
    private String instruction;
    private String imgThumb;
    private String tags;
    private String youtube;
    private String ingredients1;
    private String ingredients2;
    private String ingredients3;
    private String ingredients4;
    private String ingredients5;
    private String ingredients6;
    private String ingredients7;
    private String ingredients8;
    private String ingredients9;
    private String ingredients10;
    private String ingredients11;
    private String ingredients12;
    private String ingredients13;
    private String ingredients14;
    private String ingredients15;
    private String ingredients16;
    private String ingredients17;
    private String ingredients18;
    private String ingredients19;
    private String ingredients20;
    private String measures1;
    private String measures2;
    private String measures3;
    private String measures4;
    private String measures5;
    private String measures6;
    private String measures7;
    private String measures8;
    private String measures9;
    private String measures10;
    private String measures11;
    private String measures12;
    private String measures13;
    private String measures14;
    private String measures15;
    private String measures16;
    private String measures17;
    private String measures18;
    private String measures19;
    private String measures20;
    private String source;
    private String dateModified;

    protected Recipe(Parcel in) {
        recipeId = in.readString();
        recipeName = in.readString();
        drinkAlternate = in.readString();
        category = in.readString();
        area = in.readString();
        instruction = in.readString();
        imgThumb = in.readString();
        tags = in.readString();
        youtube = in.readString();
        ingredients1 = in.readString();
        ingredients2 = in.readString();
        ingredients3 = in.readString();
        ingredients4 = in.readString();
        ingredients5 = in.readString();
        ingredients6 = in.readString();
        ingredients7 = in.readString();
        ingredients8 = in.readString();
        ingredients9 = in.readString();
        ingredients10 = in.readString();
        ingredients11 = in.readString();
        ingredients12 = in.readString();
        ingredients13 = in.readString();
        ingredients14 = in.readString();
        ingredients15 = in.readString();
        ingredients16 = in.readString();
        ingredients17 = in.readString();
        ingredients18 = in.readString();
        ingredients19 = in.readString();
        ingredients20 = in.readString();
        measures1 = in.readString();
        measures2 = in.readString();
        measures3 = in.readString();
        measures4 = in.readString();
        measures5 = in.readString();
        measures6 = in.readString();
        measures7 = in.readString();
        measures8 = in.readString();
        measures9 = in.readString();
        measures10 = in.readString();
        measures11 = in.readString();
        measures12 = in.readString();
        measures13 = in.readString();
        measures14 = in.readString();
        measures15 = in.readString();
        measures16 = in.readString();
        measures17 = in.readString();
        measures18 = in.readString();
        measures19 = in.readString();
        measures20 = in.readString();
        source = in.readString();
        dateModified = in.readString();
    }

    public Recipe(String recipeId, String recipeName, String drinkAlternate, String category, String area, String instruction, String imgThumb, String tags, String youtube, String ingredients1, String ingredients2, String ingredients3, String ingredients4, String ingredients5, String ingredients6, String ingredients7, String ingredients8, String ingredients9, String ingredients10, String ingredients11, String ingredients12, String ingredients13, String ingredients14, String ingredients15, String ingredients16, String ingredients17, String ingredients18, String ingredients19, String ingredients20, String measures1, String measures2, String measures3, String measures4, String measures5, String measures6, String measures7, String measures8, String measures9, String measures10, String measures11, String measures12, String measures13, String measures14, String measures15, String measures16, String measures17, String measures18, String measures19, String measures20, String source, String dateModified) {
        this.recipeId = recipeId;
        this.recipeName = recipeName;
        this.drinkAlternate = drinkAlternate;
        this.category = category;
        this.area = area;
        this.instruction = instruction;
        this.imgThumb = imgThumb;
        this.tags = tags;
        this.youtube = youtube;
        this.ingredients1 = ingredients1;
        this.ingredients2 = ingredients2;
        this.ingredients3 = ingredients3;
        this.ingredients4 = ingredients4;
        this.ingredients5 = ingredients5;
        this.ingredients6 = ingredients6;
        this.ingredients7 = ingredients7;
        this.ingredients8 = ingredients8;
        this.ingredients9 = ingredients9;
        this.ingredients10 = ingredients10;
        this.ingredients11 = ingredients11;
        this.ingredients12 = ingredients12;
        this.ingredients13 = ingredients13;
        this.ingredients14 = ingredients14;
        this.ingredients15 = ingredients15;
        this.ingredients16 = ingredients16;
        this.ingredients17 = ingredients17;
        this.ingredients18 = ingredients18;
        this.ingredients19 = ingredients19;
        this.ingredients20 = ingredients20;
        this.measures1 = measures1;
        this.measures2 = measures2;
        this.measures3 = measures3;
        this.measures4 = measures4;
        this.measures5 = measures5;
        this.measures6 = measures6;
        this.measures7 = measures7;
        this.measures8 = measures8;
        this.measures9 = measures9;
        this.measures10 = measures10;
        this.measures11 = measures11;
        this.measures12 = measures12;
        this.measures13 = measures13;
        this.measures14 = measures14;
        this.measures15 = measures15;
        this.measures16 = measures16;
        this.measures17 = measures17;
        this.measures18 = measures18;
        this.measures19 = measures19;
        this.measures20 = measures20;
        this.source = source;
        this.dateModified = dateModified;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(recipeId);
        dest.writeString(recipeName);
        dest.writeString(drinkAlternate);
        dest.writeString(category);
        dest.writeString(area);
        dest.writeString(instruction);
        dest.writeString(imgThumb);
        dest.writeString(tags);
        dest.writeString(youtube);
        dest.writeString(ingredients1);
        dest.writeString(ingredients2);
        dest.writeString(ingredients3);
        dest.writeString(ingredients4);
        dest.writeString(ingredients5);
        dest.writeString(ingredients6);
        dest.writeString(ingredients7);
        dest.writeString(ingredients8);
        dest.writeString(ingredients9);
        dest.writeString(ingredients10);
        dest.writeString(ingredients11);
        dest.writeString(ingredients12);
        dest.writeString(ingredients13);
        dest.writeString(ingredients14);
        dest.writeString(ingredients15);
        dest.writeString(ingredients16);
        dest.writeString(ingredients17);
        dest.writeString(ingredients18);
        dest.writeString(ingredients19);
        dest.writeString(ingredients20);
        dest.writeString(measures1);
        dest.writeString(measures2);
        dest.writeString(measures3);
        dest.writeString(measures4);
        dest.writeString(measures5);
        dest.writeString(measures6);
        dest.writeString(measures7);
        dest.writeString(measures8);
        dest.writeString(measures9);
        dest.writeString(measures10);
        dest.writeString(measures11);
        dest.writeString(measures12);
        dest.writeString(measures13);
        dest.writeString(measures14);
        dest.writeString(measures15);
        dest.writeString(measures16);
        dest.writeString(measures17);
        dest.writeString(measures18);
        dest.writeString(measures19);
        dest.writeString(measures20);
        dest.writeString(source);
        dest.writeString(dateModified);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public String getDrinkAlternate() {
        return drinkAlternate;
    }

    public void setDrinkAlternate(String drinkAlternate) {
        this.drinkAlternate = drinkAlternate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getInstruction() {
        return instruction;
    }

    public void setInstruction(String instruction) {
        this.instruction = instruction;
    }

    public String getImgThumb() {
        return imgThumb;
    }

    public void setImgThumb(String imgThumb) {
        this.imgThumb = imgThumb;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getYoutube() {
        return youtube;
    }

    public void setYoutube(String youtube) {
        this.youtube = youtube;
    }

    public String getIngredients1() {
        return ingredients1;
    }

    public void setIngredients1(String ingredients1) {
        this.ingredients1 = ingredients1;
    }

    public String getIngredients2() {
        return ingredients2;
    }

    public void setIngredients2(String ingredients2) {
        this.ingredients2 = ingredients2;
    }

    public String getIngredients3() {
        return ingredients3;
    }

    public void setIngredients3(String ingredients3) {
        this.ingredients3 = ingredients3;
    }

    public String getIngredients4() {
        return ingredients4;
    }

    public void setIngredients4(String ingredients4) {
        this.ingredients4 = ingredients4;
    }

    public String getIngredients5() {
        return ingredients5;
    }

    public void setIngredients5(String ingredients5) {
        this.ingredients5 = ingredients5;
    }

    public String getIngredients6() {
        return ingredients6;
    }

    public void setIngredients6(String ingredients6) {
        this.ingredients6 = ingredients6;
    }

    public String getIngredients7() {
        return ingredients7;
    }

    public void setIngredients7(String ingredients7) {
        this.ingredients7 = ingredients7;
    }

    public String getIngredients8() {
        return ingredients8;
    }

    public void setIngredients8(String ingredients8) {
        this.ingredients8 = ingredients8;
    }

    public String getIngredients9() {
        return ingredients9;
    }

    public void setIngredients9(String ingredients9) {
        this.ingredients9 = ingredients9;
    }

    public String getIngredients10() {
        return ingredients10;
    }

    public void setIngredients10(String ingredients10) {
        this.ingredients10 = ingredients10;
    }

    public String getIngredients11() {
        return ingredients11;
    }

    public void setIngredients11(String ingredients11) {
        this.ingredients11 = ingredients11;
    }

    public String getIngredients12() {
        return ingredients12;
    }

    public void setIngredients12(String ingredients12) {
        this.ingredients12 = ingredients12;
    }

    public String getIngredients13() {
        return ingredients13;
    }

    public void setIngredients13(String ingredients13) {
        this.ingredients13 = ingredients13;
    }

    public String getIngredients14() {
        return ingredients14;
    }

    public void setIngredients14(String ingredients14) {
        this.ingredients14 = ingredients14;
    }

    public String getIngredients15() {
        return ingredients15;
    }

    public void setIngredients15(String ingredients15) {
        this.ingredients15 = ingredients15;
    }

    public String getIngredients16() {
        return ingredients16;
    }

    public void setIngredients16(String ingredients16) {
        this.ingredients16 = ingredients16;
    }

    public String getIngredients17() {
        return ingredients17;
    }

    public void setIngredients17(String ingredients17) {
        this.ingredients17 = ingredients17;
    }

    public String getIngredients18() {
        return ingredients18;
    }

    public void setIngredients18(String ingredients18) {
        this.ingredients18 = ingredients18;
    }

    public String getIngredients19() {
        return ingredients19;
    }

    public void setIngredients19(String ingredients19) {
        this.ingredients19 = ingredients19;
    }

    public String getIngredients20() {
        return ingredients20;
    }

    public void setIngredients20(String ingredients20) {
        this.ingredients20 = ingredients20;
    }

    public String getMeasures1() {
        return measures1;
    }

    public void setMeasures1(String measures1) {
        this.measures1 = measures1;
    }

    public String getMeasures2() {
        return measures2;
    }

    public void setMeasures2(String measures2) {
        this.measures2 = measures2;
    }

    public String getMeasures3() {
        return measures3;
    }

    public void setMeasures3(String measures3) {
        this.measures3 = measures3;
    }

    public String getMeasures4() {
        return measures4;
    }

    public void setMeasures4(String measures4) {
        this.measures4 = measures4;
    }

    public String getMeasures5() {
        return measures5;
    }

    public void setMeasures5(String measures5) {
        this.measures5 = measures5;
    }

    public String getMeasures6() {
        return measures6;
    }

    public void setMeasures6(String measures6) {
        this.measures6 = measures6;
    }

    public String getMeasures7() {
        return measures7;
    }

    public void setMeasures7(String measures7) {
        this.measures7 = measures7;
    }

    public String getMeasures8() {
        return measures8;
    }

    public void setMeasures8(String measures8) {
        this.measures8 = measures8;
    }

    public String getMeasures9() {
        return measures9;
    }

    public void setMeasures9(String measures9) {
        this.measures9 = measures9;
    }

    public String getMeasures10() {
        return measures10;
    }

    public void setMeasures10(String measures10) {
        this.measures10 = measures10;
    }

    public String getMeasures11() {
        return measures11;
    }

    public void setMeasures11(String measures11) {
        this.measures11 = measures11;
    }

    public String getMeasures12() {
        return measures12;
    }

    public void setMeasures12(String measures12) {
        this.measures12 = measures12;
    }

    public String getMeasures13() {
        return measures13;
    }

    public void setMeasures13(String measures13) {
        this.measures13 = measures13;
    }

    public String getMeasures14() {
        return measures14;
    }

    public void setMeasures14(String measures14) {
        this.measures14 = measures14;
    }

    public String getMeasures15() {
        return measures15;
    }

    public void setMeasures15(String measures15) {
        this.measures15 = measures15;
    }

    public String getMeasures16() {
        return measures16;
    }

    public void setMeasures16(String measures16) {
        this.measures16 = measures16;
    }

    public String getMeasures17() {
        return measures17;
    }

    public void setMeasures17(String measures17) {
        this.measures17 = measures17;
    }

    public String getMeasures18() {
        return measures18;
    }

    public void setMeasures18(String measures18) {
        this.measures18 = measures18;
    }

    public String getMeasures19() {
        return measures19;
    }

    public void setMeasures19(String measures19) {
        this.measures19 = measures19;
    }

    public String getMeasures20() {
        return measures20;
    }

    public void setMeasures20(String measures20) {
        this.measures20 = measures20;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }
}
