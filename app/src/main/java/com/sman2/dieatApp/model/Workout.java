package com.sman2.dieatApp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Workout implements Parcelable {

    private String workoutId;
    private String workoutName;
    private int workoutCalories;
    private int workoutMinutes;

    public Workout() {
    }

    public Workout(String workoutId, String workoutName, int workoutCalories, int workoutMinutes) {
        this.workoutId = workoutId;
        this.workoutName = workoutName;
        this.workoutCalories = workoutCalories;
        this.workoutMinutes = workoutMinutes;
    }

    public static final Creator<Workout> CREATOR = new Creator<Workout>() {
        @Override
        public Workout createFromParcel(Parcel in) {
            return new Workout(in);
        }

        @Override
        public Workout[] newArray(int size) {
            return new Workout[size];
        }
    };

    protected Workout(Parcel in) {
        workoutId = in.readString();
        workoutName = in.readString();
        workoutCalories = in.readInt();
        workoutMinutes = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(workoutId);
        dest.writeString(workoutName);
        dest.writeInt(workoutCalories);
        dest.writeInt(workoutMinutes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getWorkoutId() {
        return workoutId;
    }

    public void setWorkoutId(String workoutId) {
        this.workoutId = workoutId;
    }

    public String getWorkoutName() {
        return workoutName;
    }

    public void setWorkoutName(String workoutName) {
        this.workoutName = workoutName;
    }

    public int getWorkoutCalories() {
        return workoutCalories;
    }

    public void setWorkoutCalories(int workoutCalories) {
        this.workoutCalories = workoutCalories;
    }

    public int getWorkoutMinutes() {
        return workoutMinutes;
    }

    public void setWorkoutMinutes(int workoutMinutes) {
        this.workoutMinutes = workoutMinutes;
    }
}
