package com.sman2.dieatApp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.R;
import com.sman2.dieatApp.model.ListWorkoutByUser;
import com.sman2.dieatApp.model.Workout;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private String usernameForWorkouts;
    private List<ListWorkoutByUser> listWorkouts;
    private Context context;

    public MyAdapter(String usernameForWorkouts, List<ListWorkoutByUser> listWorkouts, Context context) {
        this.usernameForWorkouts = usernameForWorkouts;
        this.listWorkouts = listWorkouts;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_workouts, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final @NonNull ViewHolder holder, int position) {
        final ListWorkoutByUser listWorkout = listWorkouts.get(position);
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();

        dbRef.child("workouts").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    Workout tempW = ds.getValue(Workout.class);

                    if (tempW.getWorkoutId().equals(listWorkout.getWorkoutIdListWorkout())) {
                        holder.textViewActivity.setText(tempW.getWorkoutName());
                        holder.textViewDate.setText(listWorkout.getDateListWorkout());
                        holder.textViewMinutes.setText(listWorkout.getMinutesListWorkout() + " Minutes");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public int getItemCount() {
        return listWorkouts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewActivity;
        public TextView textViewDate;
        public TextView textViewMinutes;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewActivity = itemView.findViewById(R.id.textViewActivity);
            textViewDate = itemView.findViewById(R.id.textViewDate);
            textViewMinutes = itemView.findViewById(R.id.textViewMinutes);
        }
    }
}
