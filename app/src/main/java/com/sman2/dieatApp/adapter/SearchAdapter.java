package com.sman2.dieatApp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.DialogAddWorkoutFragment;
import com.sman2.dieatApp.R;
import com.sman2.dieatApp.model.Workout;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SearchAdapter extends RecyclerView.Adapter<SearchAdapter.ViewHolder> {

    private ArrayList<Workout> listWorkouts;
    private ArrayList<Workout> listWorkoutsFull;
    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";
    String TAG_WORKOUT = "TAG_WORKOUT";
    String TAG = SearchAdapter.class.getSimpleName();
    private Date currentDate;
    private Context context;

    public SearchAdapter(ArrayList<Workout> listWorkouts, Date currentDate, Context context) {
        this.listWorkouts = listWorkouts;
        listWorkoutsFull = new ArrayList<>();
        this.listWorkoutsFull.addAll(this.listWorkouts);
        this.currentDate = currentDate;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_activity_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Workout workout = listWorkouts.get(position);

        holder.workoutName.setText(workout.getWorkoutName());
        holder.workoutCalories.setText(workout.getWorkoutCalories() + " kkal");
        holder.workoutMinutes.setText(workout.getWorkoutMinutes() + " Minutes");
        holder.activityCardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager manager = ((AppCompatActivity) context).getSupportFragmentManager();
                DialogAddWorkoutFragment dialogAddWorkoutFragment = new DialogAddWorkoutFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime());
                bundle.putString(TAG_TYPE, "WORKOUT");
                bundle.putParcelable(TAG_WORKOUT, workout);
                dialogAddWorkoutFragment.setArguments(bundle);
                dialogAddWorkoutFragment.show(manager, TAG);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listWorkouts.size();
    }

    public void filter(String characterText) {
        characterText = characterText.toLowerCase(Locale.getDefault());
        listWorkouts.clear();

        if (characterText.length() == 0) {
            listWorkouts.addAll(listWorkoutsFull);
        } else {
            listWorkouts.clear();
            for (Workout workout : listWorkoutsFull) {
                if (workout.getWorkoutName().toLowerCase(Locale.getDefault()).contains(characterText)) {
                    listWorkouts.add(workout);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView workoutName;
        public TextView workoutCalories;
        public TextView workoutMinutes;
        public CardView activityCardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            workoutName = itemView.findViewById(R.id.txtActivityName);
            workoutCalories = itemView.findViewById(R.id.txtActivityCalories);
            workoutMinutes = itemView.findViewById(R.id.txtActivityTime);
            activityCardView = itemView.findViewById(R.id.activity_card_view);
        }
    }
}
