package com.sman2.dieatApp;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.sman2.dieatApp.model.Food;

import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class FoodDetailFragment extends Fragment {

    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";
    String TAG_FOOD = "TAG_FOOD";

    FloatingActionButton fabAddFood;
    Date currentDate;
    Food food;

    String type;
    private String TAG = FoodDetailFragment.class.getSimpleName();

    public FoodDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_food_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView detailName, detailBrand, detailCalories,
                detailCarbohydrate, detailDescription,
                detailFat, detailProtein, detailServing;
        super.onViewCreated(view, savedInstanceState);
        detailName = view.findViewById(R.id.detailName);
        detailBrand = view.findViewById(R.id.detailBrand);
        detailCalories = view.findViewById(R.id.detailCalories);
        detailCarbohydrate = view.findViewById(R.id.detailCarbohydrate);
        detailDescription = view.findViewById(R.id.detailDescription);
        detailFat = view.findViewById(R.id.detailFat);
        detailProtein = view.findViewById(R.id.detailProtein);
        detailServing = view.findViewById(R.id.detailServing);
        fabAddFood = view.findViewById(R.id.fabAddFood);

        if (getArguments() != null) {
            food = getArguments().getParcelable("FOOD");
            currentDate = new Date(getArguments().getLong(TAG_TIME));
            type = getArguments().getString(TAG_TYPE);
        }

        detailName.setText(food.getName());
        detailBrand.setText(food.getBrandName());
        detailProtein.setText(food.getTotalProtein());
        detailFat.setText(food.getTotalFat());
        detailCarbohydrate.setText(food.getTotalCarbohydrate());
        detailCalories.setText(food.getCalories());
        detailDescription.setText(food.getDescription());
        detailServing.setText(food.getServingSize() + " " + food.getServingUnit());


        fabAddFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                FragmentManager manager = ((AppCompatActivity)context).getSupportFragmentManager();
                DialogAddFoodFragment dialogAddFoodFragment = new DialogAddFoodFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime());
                bundle.putString(TAG_TYPE, type);
                bundle.putParcelable(TAG_FOOD, food);
                dialogAddFoodFragment.setArguments(bundle);
                dialogAddFoodFragment.show(getFragmentManager(), TAG);

            }
        });

    }
}
