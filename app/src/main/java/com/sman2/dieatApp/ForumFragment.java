package com.sman2.dieatApp;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.adapter.ForumRecyclerViewAdapter;
import com.sman2.dieatApp.model.Forum;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForumFragment extends Fragment {
    SearchView svForum;
    FloatingActionButton fab;
    String TAG = ForumFragment.class.getSimpleName();
    private ArrayList<Forum> listForums;
    private ForumRecyclerViewAdapter adapter;
    private RecyclerView rvForumList;
    private DatabaseReference databaseReference;


    public ForumFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forum, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        svForum = view.findViewById(R.id.svForum);
        fab = view.findViewById(R.id.fabAddForum);
        rvForumList = view.findViewById(R.id.rvForumList);
        rvForumList.setHasFixedSize(false);
        rvForumList.setLayoutManager(new LinearLayoutManager(getActivity()));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAddForumFragment dialogAddForumFragment = new DialogAddForumFragment();
                dialogAddForumFragment.show(getFragmentManager(), TAG);
            }
        });
        databaseReference = FirebaseDatabase.getInstance().getReference();
        listForums = new ArrayList<>();

        databaseReference.child("forums").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    listForums.add(ds.getValue(Forum.class));
                    adapter = new ForumRecyclerViewAdapter(listForums, getActivity());

                    svForum.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            String text = query.toLowerCase(Locale.getDefault());
                            adapter.filter(text);
                            rvForumList.setAdapter(adapter);
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            return false;
                        }
                    });
                    rvForumList.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private String getTimeString(Date past) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss");
//            Date past = format.parse("2016.02.05 AD at 23:59:30");
            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if (seconds < 60) {
                return (seconds + " seconds ago");
            } else if (minutes < 60) {
                return (minutes + " minutes ago");
            } else if (hours < 24) {
                return (hours + " hours ago");
            } else {
                return (days + " days ago");
            }

        } catch (Exception j) {
            j.printStackTrace();
        }
        return "";
    }
}


