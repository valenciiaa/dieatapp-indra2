package com.sman2.dieatApp;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class DiaryFragment extends Fragment {


    public DiaryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_diary, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CalendarView calendarView = (CalendarView) view.findViewById(R.id.calendarDiary);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month,
                                            int dayOfMonth) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date date = new Date();
                try {
                    date = sdf.parse(dayOfMonth + "/" + month + 1 + "/" + year);

                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Bundle bundle = new Bundle();
                bundle.putLong("TAG_TIME", date.getTime());
                Fragment mainDiaryFragment = new MainDiaryFragment();
                mainDiaryFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, mainDiaryFragment).commit();
                fragmentTransaction.addToBackStack(DiaryFragment.class.getSimpleName());

            }
        });
    }
}
