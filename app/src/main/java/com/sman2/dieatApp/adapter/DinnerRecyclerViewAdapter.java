package com.sman2.dieatApp.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.R;

public class DinnerRecyclerViewAdapter extends RecyclerView.ViewHolder {

    public TextView dinnerName;
    public TextView dinnerDesc;


    public DinnerRecyclerViewAdapter(@NonNull View itemView) {
        super(itemView);
        dinnerName = itemView.findViewById(R.id.txtDinnerName);
        dinnerDesc = itemView.findViewById(R.id.txtDinnerDesc);
    }
}
