package com.sman2.dieatApp;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sman2.dieatApp.adapter.FoodRecyclerViewAdapter;
import com.sman2.dieatApp.model.Food;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AddFoodFragment extends Fragment {

    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";

    SearchView svFood;
    Date currentDate;
    String type;

    private RecyclerView rvFoodList;
    private RecyclerView.Adapter adapter;
    private List<Food> foodList;


    public AddFoodFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_food, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        svFood = view.findViewById(R.id.svFood);
        rvFoodList = view.findViewById(R.id.rvFoodList);
        rvFoodList.setHasFixedSize(true);
        rvFoodList.setLayoutManager(new LinearLayoutManager(getActivity()));

        foodList = new ArrayList<>();
        if (getArguments() != null) {
            currentDate = new Date(getArguments().getLong(TAG_TIME));
            type = getArguments().getString(TAG_TYPE);
        }

        svFood.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                loadFood(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }

    private void loadFood(String text) {
        foodList.clear();
        String pre_url = "https://api.nutritionix.com/v1_1/search/";
        String post_url = "?results=0%3A50&cal_min=0&cal_max=50000&fields=item_name%2Cbrand_name%2Citem_id%2Cbrand_id%2Citem_description%2Cnf_protein%2Cnf_calories%2Cnf_total_carbohydrate%2Cnf_total_fat&appId=42e8cbe9&appKey=a4e373fe0f10ab1de40cffbffb9db544";
        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                pre_url + text + post_url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray array = jsonObject.getJSONArray("hits");

                    for (int i = 0; i < array.length(); ++i) {

                        JSONObject jo = array.getJSONObject(i);
                        JSONObject food = jo.getJSONObject("fields");
                        String iid = food.getString("item_id");
                        String iname = food.getString("item_name");
                        String bid = food.getString("brand_id");
                        String bname = food.getString("brand_name");
                        String ical = food.getString("nf_calories");
                        String idesc = food.getString("item_description");
                        String ifat = food.getString("nf_total_fat");
                        String iprotein = food.getString("nf_protein");
                        String icarbs = food.getString("nf_total_carbohydrate");
                        String iserve = food.getString("nf_serving_size_qty");
                        String iserveunit = food.getString("nf_serving_size_unit");

                        Food food1 = new Food(iid, iname, bid, bname, ical, idesc, ifat, iprotein, icarbs, iserve, iserveunit);

                        foodList.add(food1);

                    }

                    adapter = new FoodRecyclerViewAdapter(foodList, currentDate, type);
                    rvFoodList.setAdapter(adapter);

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }
}
