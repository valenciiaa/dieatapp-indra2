package com.sman2.dieatApp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.User;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        BottomNavigationView bottomNavigationView;
        ImageButton btnSetting;

        bottomNavigationView = findViewById(R.id.bottomNavigationView);
        btnSetting = findViewById(R.id.toolbarSetting);
        bottomNavigationView.setSelectedItemId(R.id.tab_home);
        FragmentTransaction transaction;
        loadFragment(new HomeFragment());

        btnSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Session.logoutUser();
                finish();
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(this);

        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage("Loading");
        mDialog.show();
        databaseReference.child(Session.getPrefName()).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                Session.currentUser = dataSnapshot.getValue(User.class);
                if (mDialog != null && mDialog.isShowing()) {
                    mDialog.dismiss();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        Fragment fragment = null;
        switch (menuItem.getItemId()) {
            case R.id.tab_profile:
                fragment = new ProfileFragment();
                break;
            case R.id.tab_recipe:
                fragment = new RecipeFragment();
                break;
            case R.id.tab_forum:
                fragment = new ForumFragment();
                break;
            case R.id.tab_diary:
                fragment = new DiaryFragment();
                break;
            case R.id.tab_home:
                fragment = new HomeFragment();
                break;
            default :
                break;
        }


        return loadFragment(fragment);
    }

    private boolean loadFragment(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.rootLayout, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
