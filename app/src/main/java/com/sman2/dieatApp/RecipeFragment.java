package com.sman2.dieatApp;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.sman2.dieatApp.adapter.RecipeRecyclerViewAdapter;
import com.sman2.dieatApp.model.Recipe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeFragment extends Fragment {
    private List<Recipe> recipeList;
    private RecyclerView rvRecipe;
    private RecipeRecyclerViewAdapter adapter;

    public RecipeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recipe, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        SearchView svRecipe;
        super.onViewCreated(view, savedInstanceState);
        rvRecipe = view.findViewById(R.id.rvRecipe);
        rvRecipe.setHasFixedSize(true);
        rvRecipe.setLayoutManager(new LinearLayoutManager(getActivity()));
        recipeList = new ArrayList<>();
        svRecipe = view.findViewById(R.id.svRecipe);
        loadRecipe();

        svRecipe.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                searchRecipe(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });


    }

    private void searchRecipe(String text) {
        String search_url = "https://www.themealdb.com/api/json/v2/9973533/search.php?s=";
        recipeList.clear();

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                search_url + text, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray array = jsonObject.getJSONArray("meals");

                    for (int i = 0; i < array.length(); ++i) {
                        JSONObject jo = array.getJSONObject(i);
                        String recipeId = jo.getString("idMeal");
                        String recipeName = jo.getString("strMeal");
                        String drinkAlternate = jo.getString("strDrinkAlternate");
                        String category = jo.getString("strCategory");
                        String area = jo.getString("strCategory");
                        String instruction = jo.getString("strInstructions");
                        String imgThumb = jo.getString("strMealThumb");
                        String tags = jo.getString("strTags");
                        String youtube = jo.getString("strYoutube");
                        String ingredients1 = jo.getString("strIngredient1");
                        String ingredients2 = jo.getString("strIngredient2");
                        String ingredients3 = jo.getString("strIngredient3");
                        String ingredients4 = jo.getString("strIngredient4");
                        String ingredients5 = jo.getString("strIngredient5");
                        String ingredients6 = jo.getString("strIngredient6");
                        String ingredients7 = jo.getString("strIngredient7");
                        String ingredients8 = jo.getString("strIngredient8");
                        String ingredients9 = jo.getString("strIngredient9");
                        String ingredients10 = jo.getString("strIngredient10");
                        String ingredients11 = jo.getString("strIngredient11");
                        String ingredients12 = jo.getString("strIngredient12");
                        String ingredients13 = jo.getString("strIngredient13");
                        String ingredients14 = jo.getString("strIngredient14");
                        String ingredients15 = jo.getString("strIngredient15");
                        String ingredients16 = jo.getString("strIngredient16");
                        String ingredients17 = jo.getString("strIngredient17");
                        String ingredients18 = jo.getString("strIngredient18");
                        String ingredients19 = jo.getString("strIngredient19");
                        String ingredients20 = jo.getString("strIngredient20");
                        String measures1 = jo.getString("strMeasure1");
                        String measures2 = jo.getString("strMeasure2");
                        String measures3 = jo.getString("strMeasure3");
                        String measures4 = jo.getString("strMeasure4");
                        String measures5 = jo.getString("strMeasure5");
                        String measures6 = jo.getString("strMeasure6");
                        String measures7 = jo.getString("strMeasure7");
                        String measures8 = jo.getString("strMeasure8");
                        String measures9 = jo.getString("strMeasure9");
                        String measures10 = jo.getString("strMeasure10");
                        String measures11 = jo.getString("strMeasure11");
                        String measures12 = jo.getString("strMeasure12");
                        String measures13 = jo.getString("strMeasure13");
                        String measures14 = jo.getString("strMeasure14");
                        String measures15 = jo.getString("strMeasure15");
                        String measures16 = jo.getString("strMeasure16");
                        String measures17 = jo.getString("strMeasure17");
                        String measures18 = jo.getString("strMeasure18");
                        String measures19 = jo.getString("strMeasure19");
                        String measures20 = jo.getString("strMeasure20");
                        String source = jo.getString("strSource");
                        String dateModified = jo.getString("dateModified");
                        Recipe recipe = new Recipe(recipeId, recipeName, drinkAlternate, category, area, instruction, imgThumb, tags, youtube, ingredients1, ingredients2, ingredients3, ingredients4, ingredients5, ingredients6, ingredients7, ingredients8, ingredients9, ingredients10, ingredients11, ingredients12, ingredients13, ingredients14, ingredients15, ingredients16, ingredients17, ingredients18, ingredients19, ingredients20, measures1, measures2, measures3, measures4, measures5, measures6, measures7, measures8, measures9, measures10, measures11, measures12, measures13, measures14, measures15, measures16, measures17, measures18, measures19, measures20, source, dateModified);

                        recipeList.add(recipe);


                    }

                    adapter = new RecipeRecyclerViewAdapter(recipeList);
                    rvRecipe.setAdapter(adapter);

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }

    private void loadRecipe() {
        recipeList.clear();
        String url = "https://www.themealdb.com/api/json/v2/9973533/randomselection.php";

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray array = jsonObject.getJSONArray("meals");

                    for (int i = 0; i < array.length(); ++i) {
                        JSONObject jo = array.getJSONObject(i);
                        String recipeId = jo.getString("idMeal");
                        String recipeName = jo.getString("strMeal");
                        String drinkAlternate = jo.getString("strDrinkAlternate");
                        String category = jo.getString("strCategory");
                        String area = jo.getString("strCategory");
                        String instruction = jo.getString("strInstructions");
                        String imgThumb = jo.getString("strMealThumb");
                        String tags = jo.getString("strTags");
                        String youtube = jo.getString("strYoutube");
                        String ingredients1 = jo.getString("strIngredient1");
                        String ingredients2 = jo.getString("strIngredient2");
                        String ingredients3 = jo.getString("strIngredient3");
                        String ingredients4 = jo.getString("strIngredient4");
                        String ingredients5 = jo.getString("strIngredient5");
                        String ingredients6 = jo.getString("strIngredient6");
                        String ingredients7 = jo.getString("strIngredient7");
                        String ingredients8 = jo.getString("strIngredient8");
                        String ingredients9 = jo.getString("strIngredient9");
                        String ingredients10 = jo.getString("strIngredient10");
                        String ingredients11 = jo.getString("strIngredient11");
                        String ingredients12 = jo.getString("strIngredient12");
                        String ingredients13 = jo.getString("strIngredient13");
                        String ingredients14 = jo.getString("strIngredient14");
                        String ingredients15 = jo.getString("strIngredient15");
                        String ingredients16 = jo.getString("strIngredient16");
                        String ingredients17 = jo.getString("strIngredient17");
                        String ingredients18 = jo.getString("strIngredient18");
                        String ingredients19 = jo.getString("strIngredient19");
                        String ingredients20 = jo.getString("strIngredient20");
                        String measures1 = jo.getString("strMeasure1");
                        String measures2 = jo.getString("strMeasure2");
                        String measures3 = jo.getString("strMeasure3");
                        String measures4 = jo.getString("strMeasure4");
                        String measures5 = jo.getString("strMeasure5");
                        String measures6 = jo.getString("strMeasure6");
                        String measures7 = jo.getString("strMeasure7");
                        String measures8 = jo.getString("strMeasure8");
                        String measures9 = jo.getString("strMeasure9");
                        String measures10 = jo.getString("strMeasure10");
                        String measures11 = jo.getString("strMeasure11");
                        String measures12 = jo.getString("strMeasure12");
                        String measures13 = jo.getString("strMeasure13");
                        String measures14 = jo.getString("strMeasure14");
                        String measures15 = jo.getString("strMeasure15");
                        String measures16 = jo.getString("strMeasure16");
                        String measures17 = jo.getString("strMeasure17");
                        String measures18 = jo.getString("strMeasure18");
                        String measures19 = jo.getString("strMeasure19");
                        String measures20 = jo.getString("strMeasure20");
                        String source = jo.getString("strSource");
                        String dateModified = jo.getString("dateModified");
                        Recipe recipe = new Recipe(recipeId, recipeName, drinkAlternate, category, area, instruction, imgThumb, tags, youtube, ingredients1, ingredients2, ingredients3, ingredients4, ingredients5, ingredients6, ingredients7, ingredients8, ingredients9, ingredients10, ingredients11, ingredients12, ingredients13, ingredients14, ingredients15, ingredients16, ingredients17, ingredients18, ingredients19, ingredients20, measures1, measures2, measures3, measures4, measures5, measures6, measures7, measures8, measures9, measures10, measures11, measures12, measures13, measures14, measures15, measures16, measures17, measures18, measures19, measures20, source, dateModified);
                        recipeList.add(recipe);
                    }

                    adapter = new RecipeRecyclerViewAdapter(recipeList);
                    rvRecipe.setAdapter(adapter);

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getContext(), "Error" + error.toString(), Toast.LENGTH_SHORT).show();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(stringRequest);
    }
}
