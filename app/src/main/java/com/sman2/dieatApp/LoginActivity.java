package com.sman2.dieatApp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.User;

import java.nio.charset.Charset;

public class LoginActivity extends AppCompatActivity {
    private EditText txtUserName, txtPassword;
    private Button btnLogin, btnSignUp;
    private DatabaseReference databaseReference;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }

    private void init() {
        databaseReference = FirebaseDatabase.getInstance().getReference();
        session = new Session(LoginActivity.this);
        txtUserName = findViewById(R.id.txtUsername);
        txtPassword = findViewById(R.id.txtPassword);
        btnLogin = findViewById(R.id.btnLogin);
        btnSignUp = findViewById(R.id.btnSignUp);

        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validate().isEmpty()) {
                    final ProgressDialog mDialog = new ProgressDialog(LoginActivity.this);
                    mDialog.setMessage("Please wait...");
                    mDialog.show();
                    databaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            mDialog.dismiss();
                            if (dataSnapshot.child(txtUserName.getText().toString()).exists()) {
                                User user = dataSnapshot.child(txtUserName.getText().toString()).getValue(User.class);
                                final HashCode hashCode = Hashing.sha256().hashString(txtPassword.getText().toString(), Charset.defaultCharset());
                                if (user.getPassword().equals(hashCode.toString())) {
                                    session.createLoginSession(user.getUsername());
                                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                                    startActivity(i);
                                    finish();
                                } else {
                                    Toast.makeText(LoginActivity.this, "Wrong username/password", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Toast.makeText(LoginActivity.this, "User not found", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {

                        }
                    });

                } else {
                    Toast.makeText(LoginActivity.this, validate(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private String validate() {
        if (txtUserName.getText().toString().trim().isEmpty()) {
            txtUserName.requestFocus();
            return "Username must be filled!";
        }
        if (txtPassword.getText().toString().trim().isEmpty()) {
            txtPassword.requestFocus();
            return "Password must be filled!";
        }

        return "";
    }
}
