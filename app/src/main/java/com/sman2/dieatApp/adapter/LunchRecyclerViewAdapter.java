package com.sman2.dieatApp.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.R;

public class LunchRecyclerViewAdapter extends RecyclerView.ViewHolder {
    public TextView lunchName;
    public TextView lunchDesc;


    public LunchRecyclerViewAdapter(@NonNull View itemView) {
        super(itemView);
        lunchName = itemView.findViewById(R.id.txtLunchName);
        lunchDesc = itemView.findViewById(R.id.txtLunchDesc);
    }
}
