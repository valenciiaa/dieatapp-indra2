package com.sman2.dieatApp.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.R;
import com.sman2.dieatApp.RecipeDetailFragment;
import com.sman2.dieatApp.RecipeFragment;
import com.sman2.dieatApp.model.Recipe;
import com.squareup.picasso.Picasso;

import java.util.Date;
import java.util.List;

public class RecipeRecyclerViewAdapter extends RecyclerView.Adapter<RecipeRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {
    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";
    private List<Recipe> recipeList;
    private Date currentDate;
    private String type;

    public RecipeRecyclerViewAdapter(List<Recipe> recipeList) {
        this.recipeList = recipeList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_recipe_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Recipe recipe = recipeList.get(position);
        holder.txtRecipeName.setText(recipe.getRecipeName());
        holder.txtTags.setText(recipe.getTags());
        Picasso.get().load(recipe.getImgThumb()).into(holder.img_recipe_thumb);

        holder.card_view_recipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putParcelable("RECIPE", recipe);
                Fragment recipeDetailFragment = new RecipeDetailFragment();
                recipeDetailFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.rootLayout, recipeDetailFragment).commit();
                fragmentTransaction.addToBackStack(RecipeFragment.class.getSimpleName());
            }
        });

//        holder.cardView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                AppCompatActivity activity = (AppCompatActivity) v.getContext();
//                FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
//                Bundle bundle = new Bundle();
//                bundle.putLong(TAG_TIME, currentDate.getTime());
//                bundle.putString(TAG_TYPE, type);
//                bundle.putParcelable("FOOD", food);
//                Fragment foodDetailFragment = new FoodDetailFragment();
//                foodDetailFragment.setArguments(bundle);
//                fragmentTransaction.replace(R.id.rootLayout, foodDetailFragment).commit();
//                fragmentTransaction.addToBackStack(AddFoodFragment.class.getSimpleName());
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return recipeList.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CardView card_view_recipe;
        public TextView txtRecipeName;
        public TextView txtTags;
        public ImageView img_recipe_thumb;

        public ViewHolder(View itemView) {
            super(itemView);

            txtRecipeName = (TextView) itemView.findViewById(R.id.txtRecipeName);
            txtTags = (TextView) itemView.findViewById(R.id.txtTags);
            card_view_recipe = (CardView) itemView.findViewById(R.id.card_view_recipe);
            img_recipe_thumb = itemView.findViewById(R.id.img_recipe_thumb);
        }

    }
}
