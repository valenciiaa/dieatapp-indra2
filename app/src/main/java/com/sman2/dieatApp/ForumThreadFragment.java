package com.sman2.dieatApp;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.adapter.CommentRecyclerViewAdapter;
import com.sman2.dieatApp.model.Comment;
import com.sman2.dieatApp.model.Forum;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForumThreadFragment extends Fragment {
    private static final String TAG = ForumThreadFragment.class.getSimpleName();
    private ArrayList<Comment> listComments;
    private Forum forum;
    private TextView txtForumTitle, txtForumStarter, txtForumContent;
    private RecyclerView rvComment;
    private FloatingActionButton fabAddComment;
    private DatabaseReference databaseReference;
    private CommentRecyclerViewAdapter adapter;

    public ForumThreadFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_forum_thread, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            forum = getArguments().getParcelable("FORUM");
        }
        txtForumContent = view.findViewById(R.id.txtForumContent);
        txtForumTitle = view.findViewById(R.id.txtForumTitle);
        txtForumStarter = view.findViewById(R.id.txtForumStarter);
        fabAddComment = view.findViewById(R.id.fabAddComment);
        rvComment = view.findViewById(R.id.rvComments);
        rvComment.setHasFixedSize(false);
        rvComment.setLayoutManager(new LinearLayoutManager(getActivity()));

        txtForumContent.setText(forum.getContent());
        txtForumStarter.setText("Posted by " + forum.getUserName());
        txtForumTitle.setText(forum.getTitle());
        fabAddComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAddCommentFragment dialogAddCommentFragment = new DialogAddCommentFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("FORUM", forum);
                dialogAddCommentFragment.setArguments(bundle);
                dialogAddCommentFragment.show(getFragmentManager(), TAG);
            }
        });

        databaseReference = FirebaseDatabase.getInstance().getReference();
        listComments = new ArrayList<>();

        databaseReference.child("forums").child(forum.getForumId()).child("COMMENTS").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    listComments.add(ds.getValue(Comment.class));
                    adapter = new CommentRecyclerViewAdapter(listComments);

                    rvComment.setAdapter(adapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }
}
