package com.sman2.dieatApp;


import android.graphics.Typeface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.sman2.dieatApp.model.Recipe;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 */
public class RecipeDetailFragment extends Fragment {

    YouTubePlayerView youTubePlayer;
    LinearLayout linearLayout;
    private Recipe recipe;
    private TextView txtRecipeNameDetail, txtRecipeCategory, txtRecipeDirections;


    public RecipeDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_recipe_detail, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        if (getArguments() != null) {
            recipe = getArguments().getParcelable("RECIPE");
        }
        youTubePlayer = view.findViewById(R.id.youtube_player_view);
        getLifecycle().addObserver(youTubePlayer);
        txtRecipeNameDetail = view.findViewById(R.id.txtRecipeNameDetail);
        txtRecipeCategory = view.findViewById(R.id.txtRecipeCategory);
        txtRecipeDirections = view.findViewById(R.id.txtRecipeDirections);
        linearLayout = view.findViewById(R.id.layoutRecipeIngredients);

        TextView ingredients = new TextView(getContext());
        Typeface livvic_light = getResources().getFont(R.font.livvic_black);
        ingredients.setText("Ingredients : ");
        ingredients.setTextSize(16);
        ingredients.setTypeface(livvic_light);
        linearLayout.addView(ingredients);


        txtRecipeNameDetail.setText(recipe.getRecipeName());
        txtRecipeCategory.setText(recipe.getCategory());
        txtRecipeDirections.setText(recipe.getInstruction().replace("\r", ""));


        if (recipe.getYoutube() != null || !recipe.getYoutube().isEmpty()) {
            youTubePlayer.addYouTubePlayerListener(new AbstractYouTubePlayerListener() {
                @Override
                public void onReady(@NotNull YouTubePlayer youTubePlayer) {
                    youTubePlayer.cueVideo(getYouTubeId(recipe.getYoutube()), 0);
                }
            });
        } else {
            youTubePlayer.setVisibility(View.GONE);
        }

        ArrayList<String> ingredientsList = new ArrayList<>();
        if (recipe.getIngredients1().trim() != null && !recipe.getIngredients1().trim().equals("") && !recipe.getIngredients1().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients1());
        }
        if (recipe.getIngredients2().trim() != null && !recipe.getIngredients2().trim().equals("") && !recipe.getIngredients2().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients2());
        }
        if (recipe.getIngredients3().trim() != null && !recipe.getIngredients3().trim().equals("") && !recipe.getIngredients3().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients3());
        }
        if (recipe.getIngredients4().trim() != null && !recipe.getIngredients4().trim().equals("") && !recipe.getIngredients4().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients4());
        }
        if (recipe.getIngredients5().trim() != null && !recipe.getIngredients5().trim().equals("") && !recipe.getIngredients5().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients5());
        }
        if (recipe.getIngredients6().trim() != null && !recipe.getIngredients6().trim().equals("") && !recipe.getIngredients6().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients6());
        }
        if (recipe.getIngredients7().trim() != null && !recipe.getIngredients7().trim().equals("") && !recipe.getIngredients7().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients7());
        }
        if (recipe.getIngredients8().trim() != null && !recipe.getIngredients8().trim().equals("") && !recipe.getIngredients8().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients8());
        }
        if (recipe.getIngredients9().trim() != null && !recipe.getIngredients9().trim().equals("") && !recipe.getIngredients9().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients9());
        }
        if (recipe.getIngredients10().trim() != null && !recipe.getIngredients10().trim().equals("") && !recipe.getIngredients10().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients10());

        }
        if (recipe.getIngredients11().trim() != null && !recipe.getIngredients11().trim().equals("") && !recipe.getIngredients11().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients11());
        }
        if (recipe.getIngredients12().trim() != null && !recipe.getIngredients12().trim().equals("") && !recipe.getIngredients12().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients12());
        }
        if (recipe.getIngredients13().trim() != null && !recipe.getIngredients13().trim().equals("") && !recipe.getIngredients13().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients13());
        }
        if (recipe.getIngredients14().trim() != null && !recipe.getIngredients14().trim().equals("") && !recipe.getIngredients14().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients14());
        }
        if (recipe.getIngredients15().trim() != null && !recipe.getIngredients15().trim().equals("") && !recipe.getIngredients15().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients15());
        }
        if (recipe.getIngredients16().trim() != null && !recipe.getIngredients16().trim().equals("") && !recipe.getIngredients16().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients16());
        }
        if (recipe.getIngredients17().trim() != null && !recipe.getIngredients17().trim().equals("") && !recipe.getIngredients17().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients17());
        }
        if (recipe.getIngredients18().trim() != null && !recipe.getIngredients18().trim().equals("") && !recipe.getIngredients18().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients18());
        }
        if (recipe.getIngredients19().trim() != null && !recipe.getIngredients19().trim().equals("") && !recipe.getIngredients19().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients19());
        }
        if (recipe.getIngredients20().trim() != null && !recipe.getIngredients20().trim().equals("") && !recipe.getIngredients20().trim().equals("null")) {
            ingredientsList.add(recipe.getIngredients20());
        }

        ArrayList<String> measuresList = new ArrayList<>();
        if (recipe.getMeasures1().trim() != null && !recipe.getMeasures1().trim().equals("") && !recipe.getMeasures1().trim().equals("null")) {
            measuresList.add(recipe.getMeasures1());
        }
        if (recipe.getMeasures2().trim() != null && !recipe.getMeasures2().trim().equals("") && !recipe.getMeasures2().trim().equals("null")) {
            measuresList.add(recipe.getMeasures2());
        }
        if (recipe.getMeasures3().trim() != null && !recipe.getMeasures3().trim().equals("") && !recipe.getMeasures3().trim().equals("null")) {
            measuresList.add(recipe.getMeasures3());
        }
        if (recipe.getMeasures4().trim() != null && !recipe.getMeasures4().trim().equals("") && !recipe.getMeasures4().trim().equals("null")) {
            measuresList.add(recipe.getMeasures4());
        }
        if (recipe.getMeasures5().trim() != null && !recipe.getMeasures5().trim().equals("") && !recipe.getMeasures5().trim().equals("null")) {
            measuresList.add(recipe.getMeasures5());
        }
        if (recipe.getMeasures6().trim() != null && !recipe.getMeasures6().trim().equals("") && !recipe.getMeasures6().trim().equals("null")) {
            measuresList.add(recipe.getMeasures6());
        }
        if (recipe.getMeasures7().trim() != null && !recipe.getMeasures7().trim().equals("") && !recipe.getMeasures7().trim().equals("null")) {
            measuresList.add(recipe.getMeasures7());
        }
        if (recipe.getMeasures8().trim() != null && !recipe.getMeasures8().trim().equals("") && !recipe.getMeasures8().trim().equals("null")) {
            measuresList.add(recipe.getMeasures8());
        }
        if (recipe.getMeasures9().trim() != null && !recipe.getMeasures9().trim().equals("") && !recipe.getMeasures9().trim().equals("null")) {
            measuresList.add(recipe.getMeasures9());
        }
        if (recipe.getMeasures10().trim() != null && !recipe.getMeasures10().trim().equals("") && !recipe.getMeasures10().trim().equals("null")) {
            measuresList.add(recipe.getMeasures10());

        }
        if (recipe.getMeasures11().trim() != null && !recipe.getMeasures11().trim().equals("") && !recipe.getMeasures11().trim().equals("null")) {
            measuresList.add(recipe.getMeasures11());
        }
        if (recipe.getMeasures12().trim() != null && !recipe.getMeasures12().trim().equals("") && !recipe.getMeasures12().trim().equals("null")) {
            measuresList.add(recipe.getMeasures12());
        }
        if (recipe.getMeasures13().trim() != null && !recipe.getMeasures13().trim().equals("") && !recipe.getMeasures13().trim().equals("null")) {
            measuresList.add(recipe.getMeasures13());
        }
        if (recipe.getMeasures14().trim() != null && !recipe.getMeasures14().trim().equals("") && !recipe.getMeasures14().trim().equals("null")) {
            measuresList.add(recipe.getMeasures14());
        }
        if (recipe.getMeasures15().trim() != null && !recipe.getMeasures15().trim().equals("") && !recipe.getMeasures15().trim().equals("null")) {
            measuresList.add(recipe.getMeasures15());
        }
        if (recipe.getMeasures16().trim() != null && !recipe.getMeasures16().trim().equals("") && !recipe.getMeasures16().trim().equals("null")) {
            measuresList.add(recipe.getMeasures16());
        }
        if (recipe.getMeasures17().trim() != null && !recipe.getMeasures17().trim().equals("") && !recipe.getMeasures17().trim().equals("null")) {
            measuresList.add(recipe.getMeasures17());
        }
        if (recipe.getMeasures18().trim() != null && !recipe.getMeasures18().trim().equals("") && !recipe.getMeasures18().trim().equals("null")) {
            measuresList.add(recipe.getMeasures18());
        }
        if (recipe.getMeasures19().trim() != null && !recipe.getMeasures19().trim().equals("") && !recipe.getMeasures19().trim().equals("null")) {
            measuresList.add(recipe.getMeasures19());
        }
        if (recipe.getMeasures20().trim() != null && !recipe.getMeasures20().trim().equals("") && !recipe.getMeasures20().trim().equals("null")) {
            measuresList.add(recipe.getMeasures20());
        }


        for (int i = 0; i < ingredientsList.size(); i++) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView newTextView = new TextView(getContext());
            params.setMargins(20, 10, 20, 0);
            newTextView.setText(measuresList.get(i) + " " + ingredientsList.get(i));
            newTextView.setLayoutParams(params);
            linearLayout.addView(newTextView);

        }
    }

    private String getYouTubeId(String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "error";
        }
    }
}
