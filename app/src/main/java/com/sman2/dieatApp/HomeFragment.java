package com.sman2.dieatApp;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.anastr.speedviewlib.SpeedView;
import com.github.anastr.speedviewlib.components.Section;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataSource;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.sman2.dieatApp.common.Session;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import static android.app.Activity.RESULT_OK;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    private static final int REQUEST_SPEECH = 1;
    SpeedView speedView;
    TextView txtHomeExercise;
    Button btnTrophies;
    ImageView btnVoice;

    private GoogleApiClient mGoogleApiClient;


    public HomeFragment() {
        // Required empty public constructor

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btnVoice = view.findViewById(R.id.btnVoice);
        speedView = view.findViewById(R.id.speedoMeter);
        txtHomeExercise = view.findViewById(R.id.txtHomeExercise);
        btnTrophies = view.findViewById(R.id.btnTrophies);
        btnVoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Choose Activity");
                startActivityForResult(intent, REQUEST_SPEECH);
            }
        });

        speedView.clearSections();

        speedView.addSections(new Section(.33f, Color.parseColor("#EE485E")),
                new Section(.66f, Color.parseColor("#21B789")),
                new Section(1, Color.parseColor("#EE485E")));
        speedView.speedPercentTo(50, 1000);


    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addApi(Fitness.HISTORY_API)
                    .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ_WRITE))
                    .addScope(new Scope(Scopes.FITNESS_BODY_READ))
                    .addScope(new Scope(Scopes.FITNESS_BODY_READ_WRITE))
                    .addConnectionCallbacks(this)
                    .enableAutoManage(getActivity(), 0, this)
                    .build();

        }
        new ReadDataDaily().execute();

    }

    @Override
    public void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.stopAutoManage(getActivity());
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.e("HistoryAPI", "onConnected");
        long startTime = Calendar.getInstance().getTimeInMillis();
        long endTime = startTime;
        if (mGoogleApiClient == null) {

            DataSet weightDataSet = createDataForRequest(
                    DataType.TYPE_WEIGHT,    // for height, it would be DataType.TYPE_HEIGHT
                    DataSource.TYPE_RAW,
                    Session.currentUser.getWeight(),                  // weight in kgs
                    startTime,              // start time
                    endTime,                // end time
                    TimeUnit.MILLISECONDS                // Time Unit, for example, TimeUnit.MILLISECONDS
            );

            DataSet heightDataSet = createDataForRequest(
                    DataType.TYPE_HEIGHT,    // for height, it would be DataType.TYPE_HEIGHT
                    DataSource.TYPE_RAW,
                    Session.currentUser.getHeight(),                  // weight in kgs
                    startTime,              // start time
                    endTime,                // end time
                    TimeUnit.MILLISECONDS                // Time Unit, for example, TimeUnit.MILLISECONDS
            );

            Fitness.HistoryApi.insertData(mGoogleApiClient, weightDataSet)
                    .await(1, TimeUnit.MINUTES);

            Fitness.HistoryApi.insertData(mGoogleApiClient, heightDataSet)
                    .await(1, TimeUnit.MINUTES);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.e("HistoryAPI", "onConnectionSuspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.e("HistoryAPI", "onConnectionFailed");
    }


    private void showDataSet(DataSet dataSet) {
        Log.e("History", "Data returned for Data type: " + dataSet.getDataType().getName());
        DateFormat dateFormat = DateFormat.getDateInstance();
        DateFormat timeFormat = DateFormat.getTimeInstance();

        for (final DataPoint dp : dataSet.getDataPoints()) {
            Log.e("History", "Data point:");
            Log.e("History", "\tType: " + dp.getDataType().getName());
            Log.e("History", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            Log.e("History", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            for (final Field field : dp.getDataType().getFields()) {
                Log.e("History", "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        txtHomeExercise.setText(String.format("%.0f", dp.getValue(field).asFloat()));
                    }
                });

            }
        }
    }

    private void displayStepDataForToday() {
        DailyTotalResult result = Fitness.HistoryApi.readDailyTotal(mGoogleApiClient, DataType.TYPE_CALORIES_EXPENDED).await(1, TimeUnit.MINUTES);
        showDataSet(result.getTotal());
    }

    private void dumpDataSet(DataSet dataSet) {
        Log.e("History", "Data returned for Data type: " + dataSet.getDataType().getName());
        DateFormat dateFormat = DateFormat.getDateInstance();
        DateFormat timeFormat = DateFormat.getTimeInstance();

        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.e("History", "Data point:");
            Log.e("History", "\tType: " + dp.getDataType().getName());
            Log.e("History", "\tStart: " + dateFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            Log.e("History", "\tEnd: " + dateFormat.format(dp.getEndTime(TimeUnit.MILLISECONDS)) + " " + timeFormat.format(dp.getStartTime(TimeUnit.MILLISECONDS)));
            for (Field field : dp.getDataType().getFields()) {
                Log.e("History", "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));
            }
        }
//        for (DataPoint dp : dataSet.getDataPoints()) {
//            List<Field> field = dp.getDataType().getFields();
//            if (field.get(0).getName().equalsIgnoreCase("activity")) {
//                if (dp.getValue(field.get(0)).asActivity()
//                        .equalsIgnoreCase("walking")) {
//                    walkingTime = TimeUnit.MILLISECONDS.toSeconds(dp.getValue(
//                            field.get(1)).asInt());
//
//                    Log.e("walking", walkingTime + "");
//                }
//
//                if (dp.getValue(field.get(0)).asActivity()
//                        .equalsIgnoreCase("biking")) {
//                    cyclingTime = TimeUnit.MILLISECONDS.toSeconds(dp
//                            .getValue(field.get(1)).asInt());
//
//                    Log.e("biking", cyclingTime + "");
//                }
//                if (dp.getValue(field.get(0)).asActivity()
//                        .equalsIgnoreCase("in_vehicle")) {
//                    travelingTime = TimeUnit.MILLISECONDS.toSeconds(dp
//                            .getValue(field.get(1)).asInt());
//                    Log.e("in_vehicle", travelingTime + "");
//                }
//                if (dp.getValue(field.get(0)).asActivity()
//                        .contains("running")) {
//                    runningTime = TimeUnit.MILLISECONDS.toSeconds(dp
//                            .getValue(field.get(1)).asInt());
//
//                    Log.e("running", runningTime + "");
//                }
//            } else if (field.get(0).getName().equalsIgnoreCase("steps")) {
//
//                steps = dp.getValue(field.get(0)).asInt();
//
//                Log.e("steps", "" + steps);
//            }
//        }
    }

    private DataSet createDataForRequest(DataType dataType, int dataSourceType, int values,
                                         long startTime, long endTime, TimeUnit timeUnit) {
        DataSource dataSource = new DataSource.Builder()
                .setAppPackageName(getActivity())
                .setDataType(dataType)
                .setType(dataSourceType)
                .build();

        DataSet dataSet = DataSet.create(dataSource);
        DataPoint dataPoint = dataSet.createDataPoint().setTimeInterval(startTime, endTime, timeUnit);

        dataPoint = dataPoint.setIntValues((Integer) values);

        dataSet.add(dataPoint);

        return dataSet;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == REQUEST_SPEECH) {
            if (resultCode == RESULT_OK) {
                ArrayList<String> matches = data
                        .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);

                if (matches.size() == 0) {
                    // didn't hear anything
                } else {
                    String mostLikelyThingHeard = matches.get(0);
                    // toUpperCase() used to make string comparison equal
                    if (mostLikelyThingHeard.toUpperCase().equals("DIARY")) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.rootLayout, new DiaryFragment())
                                .commit();
                    } else if (mostLikelyThingHeard.toUpperCase().equals("FORUM")) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.rootLayout, new ForumFragment())
                                .commit();
                    } else if (mostLikelyThingHeard.toUpperCase().equals("RECIPE")) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.rootLayout, new RecipeFragment())
                                .commit();
                    } else if (mostLikelyThingHeard.toUpperCase().equals("PROFILE")) {
                        getActivity().getSupportFragmentManager().beginTransaction()
                                .replace(R.id.rootLayout, new ProfileFragment())
                                .commit();
                    }
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    private class ReadDataDaily extends AsyncTask<Void, Void, Void> {

        protected Void doInBackground(Void... params) {
            displayStepDataForToday();
            return null;
        }
    }


}
