package com.sman2.dieatApp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Forum implements Parcelable {
    public static final Creator<Forum> CREATOR = new Creator<Forum>() {
        @Override
        public Forum createFromParcel(Parcel in) {
            return new Forum(in);
        }

        @Override
        public Forum[] newArray(int size) {
            return new Forum[size];
        }
    };
    private String title;
    private String userName;
    private int totalComments;
    private String createdAt;
    private String content;
    private String forumId;

    protected Forum(Parcel in) {
        title = in.readString();
        userName = in.readString();
        totalComments = in.readInt();
        createdAt = in.readString();
        content = in.readString();
        forumId = in.readString();
    }

    public Forum() {
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(userName);
        dest.writeInt(totalComments);
        dest.writeString(createdAt);
        dest.writeString(content);
        dest.writeString(forumId);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getTotalComments() {
        return totalComments;
    }

    public void setTotalComments(int totalComments) {
        this.totalComments = totalComments;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getForumId() {
        return forumId;
    }

    public void setForumId(String forumId) {
        this.forumId = forumId;
    }


}
