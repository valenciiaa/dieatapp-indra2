package com.sman2.dieatApp;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.User;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static android.text.TextUtils.isEmpty;


/**
 * A simple {@link Fragment} subclass.
 */
public class EditProfileFragment extends Fragment {

    final Calendar myCalendarEditProfile = Calendar.getInstance();
    private EditText fullNameEditProfile;
    private EditText emailEditProfile;
    private EditText dobEditProfile;
    private EditText weightEditProfile;
    private EditText heightEditProfile;
    private Button editProfileSubmit;
    private DatePickerDialog.OnDateSetListener dateEditProfile;

    public EditProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_edit_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fullNameEditProfile = view.findViewById(R.id.fullNameEditProfileTxt);
        emailEditProfile = view.findViewById(R.id.emailEditProfileTxt);
        dobEditProfile = view.findViewById(R.id.dobEditProfileTxt);
        weightEditProfile = view.findViewById(R.id.weightEditProfileTxt);
        heightEditProfile = view.findViewById(R.id.heightEditProfileTxt);
        editProfileSubmit = view.findViewById(R.id.editProfileSubmitBtn);

        fullNameEditProfile.setText(Session.currentUser.getFullName());
        emailEditProfile.setText(Session.currentUser.getEmail());
        dobEditProfile.setText(Session.currentUser.getDob());
        weightEditProfile.setText(Session.currentUser.getWeight() + "");
        heightEditProfile.setText(Session.currentUser.getHeight() + "");

        dateEditProfile = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendarEditProfile.set(Calendar.YEAR, year);
                myCalendarEditProfile.set(Calendar.MONTH, month);
                myCalendarEditProfile.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };

        dobEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(EditProfileFragment.super.getContext(), dateEditProfile,
                        myCalendarEditProfile.get(Calendar.YEAR),
                        myCalendarEditProfile.get(Calendar.MONTH),
                        myCalendarEditProfile.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        editProfileSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String errorMessage = validateInput();

                if (errorMessage.length() == 0) {
                    User userTemp = Session.currentUser;
                    String fullNameTemp = fullNameEditProfile.getText().toString();
                    String dobTemp = dobEditProfile.getText().toString();
                    int weightTemp = Integer.parseInt(weightEditProfile.getText().toString());
                    int heightTemp = Integer.parseInt(heightEditProfile.getText().toString());
                    int flag = 0;

                    if (!userTemp.getFullName().equals(fullNameTemp)) {
                        userTemp.setFullName(fullNameTemp);
                        flag++;
                    }
                    if (!userTemp.getDob().equals(dobTemp)) {
                        userTemp.setDob(dobTemp);
                        flag++;
                    }
                    if (!(userTemp.getWeight() == weightTemp)) {
                        userTemp.setWeight(weightTemp);
                        flag++;
                    }
                    if (!(userTemp.getHeight() == heightTemp)) {
                        userTemp.setHeight(heightTemp);
                        flag++;
                    }

                    if (flag != 0) {
                        updateUserProfile(userTemp);
                    }

                    returnToProfileFragment();
                } else {
                    Toast.makeText(EditProfileFragment.super.getContext(), errorMessage, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dobEditProfile.setText(sdf.format(myCalendarEditProfile.getTime()));
    }

    private void updateUserProfile(final User updatedUser) {
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        final Query query = databaseReference.equalTo(updatedUser.getUsername());
        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    databaseReference.child(updatedUser.getUsername()).child("dob").setValue(updatedUser.getDob());
                    databaseReference.child(updatedUser.getUsername()).child("email").setValue(updatedUser.getEmail());
                    databaseReference.child(updatedUser.getUsername()).child("fullName").setValue(updatedUser.getFullName());
                    databaseReference.child(updatedUser.getUsername()).child("height").setValue(updatedUser.getHeight());
                    databaseReference.child(updatedUser.getUsername()).child("weight").setValue(updatedUser.getWeight());
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
//        databaseReference.child("users").child(Session.currentUser.getUsername()).setValue(updatedUser);
    }

    private void returnToProfileFragment() {
        Fragment fragment = new ProfileFragment();
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.rootLayout, fragment).commit();
        fragmentTransaction.addToBackStack(EditProfileFragment.class.getSimpleName());
    }

    private String validateInput() {
        String errMsg = "";
        if (isEmpty(fullNameEditProfile.getText().toString()) ||
                isEmpty(dobEditProfile.getText().toString()) ||
                isEmpty(weightEditProfile.getText().toString()) ||
                isEmpty(heightEditProfile.getText().toString())) {
            errMsg = "All field must be filled!";
        } else if (fullNameEditProfile.length() > 30) {
            errMsg = "Full name maximum 30 characters!";
        } else if (Integer.parseInt(weightEditProfile.getText().toString()) < 1) {
            errMsg = "Weight must be > 0";
        } else if (Integer.parseInt(heightEditProfile.getText().toString()) < 1) {
            errMsg = "Height must be > 0";
        }
        return errMsg;
    }
}
