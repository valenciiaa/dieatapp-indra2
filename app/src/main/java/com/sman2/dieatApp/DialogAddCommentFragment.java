package com.sman2.dieatApp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.Comment;
import com.sman2.dieatApp.model.Forum;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogAddCommentFragment extends DialogFragment {
    Forum forum;
    String TAG_WORKOUT = "TAG_WORKOUT";
    private EditText txtAddComment;
    private Context context;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        if (getArguments() != null) {
            forum = getArguments().getParcelable("FORUM");
        }

        View view = inflater.inflate(R.layout.dialog_add_comment, null);

        builder.setView(view).setTitle("Input comment ").setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        }).setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doAddComment();
                dialog.dismiss();
            }
        });
        txtAddComment = (EditText) view.findViewById(R.id.txtAddComment);


        return builder.create();
    }

    private void doAddComment() {
        final String content = txtAddComment.getText().toString();
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_forums = database.getReference("forums").child(forum.getForumId()).child("COMMENTS");
        final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Please wait...");
        mDialog.show();
        table_forums.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Comment comment = new Comment();
                comment.setContent(content);
                comment.setDislike(0);
                comment.setLike(0);
                comment.setTotalLike(0);
                comment.setUserName(Session.currentUser.getUsername());
                comment.setCreatedAt(sdf.format(new Date()));
                table_forums.push().setValue(comment);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
