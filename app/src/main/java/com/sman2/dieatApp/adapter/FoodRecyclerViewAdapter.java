package com.sman2.dieatApp.adapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.AddFoodFragment;
import com.sman2.dieatApp.FoodDetailFragment;
import com.sman2.dieatApp.R;
import com.sman2.dieatApp.model.Food;

import java.util.Date;
import java.util.List;

public class FoodRecyclerViewAdapter extends RecyclerView.Adapter<FoodRecyclerViewAdapter.ViewHolder> implements View.OnClickListener {
    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";
    private List<Food> foodList;
    private Date currentDate;
    private String type;

    public FoodRecyclerViewAdapter(List<Food> foodList, Date date, String type) {
        this.foodList = foodList;
        this.currentDate = date;
        this.type = type;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_food_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Food food = foodList.get(position);
        holder.txtFoodName.setText(food.getBrandName() + ", " + food.getName());
        holder.txtFoodDesc.setText(food.getCalories() + " cal per " + food.getServingSize() + " " + food.getServingUnit());

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();
                FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime());
                bundle.putString(TAG_TYPE, type);
                bundle.putParcelable("FOOD", food);
                Fragment foodDetailFragment = new FoodDetailFragment();
                foodDetailFragment.setArguments(bundle);
                fragmentTransaction.replace(R.id.rootLayout, foodDetailFragment).commit();
                fragmentTransaction.addToBackStack(AddFoodFragment.class.getSimpleName());
            }
        });

    }

    @Override
    public int getItemCount() {
        return foodList.size();
    }

    @Override
    public void onClick(View v) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public CardView cardView;
        public TextView txtFoodName;
        public TextView txtFoodDesc;

        public ViewHolder(View itemView) {
            super(itemView);

            txtFoodName = (TextView) itemView.findViewById(R.id.txtFoodName);
            txtFoodDesc = (TextView) itemView.findViewById(R.id.txtFoodDesc);
            cardView = (CardView) itemView.findViewById(R.id.card_view);
        }

    }
}
