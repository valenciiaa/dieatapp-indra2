package com.sman2.dieatApp.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class User implements Serializable {

    private String fullName;
    private String photoUrl;
    private String username;
    private String email;
    private String password;
    private String dob;
    private String gender;
    private int weight;
    private int height;
    private HashMap<String, HashMap<String, ListWorkoutByUser>> listUserWorkout;

    public User() {
    }

    public User(String fullName, String photoUrl, String username, String email, String password, String dob, String gender, int weight, int height, HashMap<String, HashMap<String, ListWorkoutByUser>> listUserWorkout) {
        this.fullName = fullName;
        this.photoUrl = photoUrl;
        this.username = username;
        this.email = email;
        this.password = password;
        this.dob = dob;
        this.gender = gender;
        this.weight = weight;
        this.height = height;
        this.listUserWorkout = listUserWorkout;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public HashMap getListUserWorkout() {
        return listUserWorkout;
    }

    public void setListUserWorkout(HashMap<String, HashMap<String, ListWorkoutByUser>> listUserWorkout) {
        this.listUserWorkout = listUserWorkout;
    }

    public ArrayList<ListWorkoutByUser> getListWorkout(String keyDate) {
        if (listUserWorkout != null) {
            HashMap<String, ListWorkoutByUser> values = listUserWorkout.get(keyDate);
            if (!(values == null)) {
                Collection<ListWorkoutByUser> listValue = values.values();
                return new ArrayList<>(listValue);
            }
        }
        return null;
    }
}
