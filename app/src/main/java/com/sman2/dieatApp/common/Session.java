package com.sman2.dieatApp.common;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.LoginActivity;
import com.sman2.dieatApp.MainActivity;
import com.sman2.dieatApp.model.User;

public class Session {

    public static User currentUser;

    // Shared Preferences
    static SharedPreferences pref;

    // Editor for Shared preferences
    static SharedPreferences.Editor editor;

    // Context
    static Activity _activity;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "Pref";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";

    // User name (make variable public to access from outside)
    public static final String KEY_USERNAME = "userName";

    // Constructor
    public Session(Activity activity) {
        this._activity = activity;
        pref = _activity.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Clear session details
     */
    public static void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_activity, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _activity.startActivity(i);
    }

    public static String getPrefName() {
        return pref.getString(KEY_USERNAME, null);
    }

    /**
     * Create login session
     */
    public void createLoginSession(String userName) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing name in pref
        editor.putString(KEY_USERNAME, userName);

        // commit changes
        editor.commit();

//        getUserDetails();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        Intent i;
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            i = new Intent(_activity, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        } else {
//            getUserDetails();

            i = new Intent(_activity, MainActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


        }
        _activity.startActivity(i);


    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Get stored session data
     */
    public void getUserDetails() {
//        final ProgressDialog mDialog = new ProgressDialog(_activity);
//        mDialog.setMessage("Please wait...");
//        mDialog.show();
        final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("users");

        databaseReference.child(pref.getString(KEY_USERNAME, null)).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                currentUser = dataSnapshot.getValue(User.class);
//                if (mDialog != null && mDialog.isShowing()) {
//                    mDialog.dismiss();
//                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }
}
