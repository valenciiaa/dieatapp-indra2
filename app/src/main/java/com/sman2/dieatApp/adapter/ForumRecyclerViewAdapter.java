package com.sman2.dieatApp.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.ForumFragment;
import com.sman2.dieatApp.ForumThreadFragment;
import com.sman2.dieatApp.R;
import com.sman2.dieatApp.model.Forum;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class ForumRecyclerViewAdapter extends RecyclerView.Adapter<ForumRecyclerViewAdapter.ViewHolder> {
    private ArrayList<Forum> listForum;
    private ArrayList<Forum> listForumFull;
    private Context context;

    public ForumRecyclerViewAdapter(ArrayList<Forum> listForum, Context context) {
        this.listForum = listForum;
        listForumFull = new ArrayList<>();
        this.listForumFull.addAll(this.listForum);
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_forum_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Forum forum = listForum.get(position);

        holder.txtForumTitle.setText(forum.getTitle());
        holder.txtForumCreatedBy.setText("Posted by " + forum.getUserName());
        holder.txtComment.setText(forum.getTotalComments() + " comments");
        holder.txtForumDate.setText(getTimeString(forum.getCreatedAt()));
        holder.cardViewForum.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ForumThreadFragment forumThreadFragment = new ForumThreadFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("FORUM", forum);
                forumThreadFragment.setArguments(bundle);
                FragmentTransaction transaction = ((AppCompatActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.rootLayout, forumThreadFragment).commit();
                transaction.addToBackStack(ForumFragment.class.getSimpleName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return listForum.size();
    }

    public void filter(String characterText) {
        characterText = characterText.toLowerCase(Locale.getDefault());
        listForum.clear();

        if (characterText.length() == 0) {
            listForum.addAll(listForumFull);
        } else {
            listForum.clear();
            for (Forum forum : listForumFull) {
                if (forum.getTitle().toLowerCase(Locale.getDefault()).contains(characterText)) {
                    listForum.add(forum);
                }
            }
        }
        notifyDataSetChanged();
    }

    private String getTimeString(String pastString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date past = format.parse(pastString);
            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if (seconds < 60) {
                return (seconds + " seconds ago");
            } else if (minutes < 60) {
                return (minutes + " minutes ago");
            } else if (hours < 24) {
                return (hours + " hours ago");
            } else {
                return (days + " days ago");
            }

        } catch (Exception j) {
            j.printStackTrace();
        }
        return "";
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtForumTitle;
        public TextView txtForumCreatedBy;
        public TextView txtForumDate;
        public TextView txtComment;
        public CardView cardViewForum;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtForumTitle = itemView.findViewById(R.id.txtForumTitle);
            txtForumCreatedBy = itemView.findViewById(R.id.txtForumCreatedBy);
            txtForumDate = itemView.findViewById(R.id.txtForumDate);
            txtComment = itemView.findViewById(R.id.txtComment);
            cardViewForum = itemView.findViewById(R.id.card_view_forum);
        }
    }
}
