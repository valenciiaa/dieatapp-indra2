package com.sman2.dieatApp;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.snackbar.Snackbar;
import com.google.common.hash.HashCode;
import com.google.common.hash.Hashing;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sman2.dieatApp.model.User;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class RegisterActivity extends AppCompatActivity {

    private DatabaseReference databaseReference;
    User user;

    private Button tempRegisterBtn;
    private Button tempCancelBtn;
    private EditText fullname;
    private EditText username;
    private EditText email;
    private EditText password;
    private EditText confirmationPassword;
    private EditText dob;
    private String gender;
    private RadioGroup tempRadioGroup;
    private RadioButton tempRadioBtn;
    private EditText weight;
    private EditText height;
    private int tempCheck;
    StorageReference storageRef;
    private LinearLayout registerAddPhoto;
    private ImageView ivPhoto;
    final Calendar myCalendar = Calendar.getInstance();
    private DatePickerDialog.OnDateSetListener date;
    private Uri uri;
    private ProgressDialog mProgressDialog;

    private String errMsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        databaseReference = FirebaseDatabase.getInstance().getReference();
        storageRef = FirebaseStorage.getInstance().getReference();

        fullname = findViewById(R.id.fullNameTxt);
        username = findViewById(R.id.usernameTxt);
        email = findViewById(R.id.emailTxt);
        password = findViewById(R.id.passwordTxt);
        confirmationPassword = findViewById(R.id.confirmationPasswordTxt);
        dob = findViewById(R.id.dobTxt);
        registerAddPhoto = findViewById(R.id.registerAddPhoto);
        ivPhoto = findViewById(R.id.ivPhoto);

        date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, month);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }
        };
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                errMsg = "";
                new DatePickerDialog(RegisterActivity.this, date, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        tempRadioGroup = findViewById(R.id.radioBtnGroup);
        tempRadioBtn = findViewById(tempRadioGroup.getCheckedRadioButtonId());
        tempRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.maleRadioBtn) tempRadioBtn = findViewById(R.id.maleRadioBtn);
                else if (checkedId == R.id.femaleRadioBtn)
                    tempRadioBtn = findViewById(R.id.femaleRadioBtn);
                gender = tempRadioBtn.getText().toString();
            }
        });
        weight = findViewById(R.id.weightTxt);
        height = findViewById(R.id.heightTxt);

        tempRegisterBtn = findViewById(R.id.registerBtn);
        tempCancelBtn = findViewById(R.id.cancelRegisterBtn);

        tempCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnToLogin();
            }
        });

        tempRegisterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateDataAndInputToDB(email.getText().toString(), username.getText().toString());
            }
        });

        registerAddPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dexter.withActivity(RegisterActivity.this)
                        .withPermissions(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    showImagePickerOptions(200);
                                }

                                if (report.isAnyPermissionPermanentlyDenied()) {
                                    showSettingsDialog();
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }
        });

    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Get Permission");
        builder.setMessage("Permission Message");
        builder.setPositiveButton("Go to setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton(getString(android.R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }

    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", this.getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void returnToLogin() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    private void insertUserToDB(User user) {
        databaseReference.child("users").child(user.getUsername()).setValue(user).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                returnToLogin();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dob.setText(sdf.format(myCalendar.getTime()));
    }

    private boolean isEmpty(String s) {
        return TextUtils.isEmpty(s);
    }

    private boolean isValidEmail(CharSequence target) {
        return Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void validateDataAndInputToDB(final String emailCheck, final String usernameCheck) {
        databaseReference.child("users").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                tempCheck = 0;
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    User userCheck = ds.getValue(User.class);

                    if (userCheck.getEmail().equals(emailCheck)) tempCheck = 1;
                    else if (userCheck.getUsername().equals(usernameCheck)) tempCheck = 2;
                }
                if (validateInput() == 0) {
                    final HashCode hashCode = Hashing.sha256().hashString(password.getText().toString(), Charset.defaultCharset());
//                    insertUserToDB(new User(fullname.getText().toString(), username.getText().toString(), email.getText().toString(),
//                            hashCode.toString(), dob.getText().toString(), gender, Integer.parseInt(weight.getText().toString()),
//                            Integer.parseInt(height.getText().toString())));
                    user = new User();
                    user.setFullName(fullname.getText().toString());
                    user.setUsername(username.getText().toString());
                    user.setEmail(email.getText().toString());
                    user.setPassword(hashCode.toString());
                    user.setDob(dob.getText().toString());
                    user.setGender(gender);
                    user.setWeight(Integer.parseInt(weight.getText().toString()));
                    user.setHeight(Integer.parseInt(height.getText().toString()));
                    if (uri != null) {
                        doUpload(uri);
                    } else {
                        doUpload(Uri.parse("android.resource://" + getApplicationContext().getPackageName() + "/drawable/myimage"));
                    }
//                    insertUserToDB(user);
                } else {
                    Snackbar.make(findViewById(R.id.registerBtn), errMsg, Snackbar.LENGTH_LONG).show();
                }
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(fullname.getWindowToken(), 0);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Snackbar.make(findViewById(R.id.registerBtn), "Database Error!", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private int validateInput() {
        if (isEmpty(fullname.getText().toString()) ||
                isEmpty(email.getText().toString()) ||
                isEmpty(password.getText().toString()) ||
                isEmpty(confirmationPassword.getText().toString()) ||
                isEmpty(dob.getText().toString()) ||
                isEmpty(gender) ||
                isEmpty(weight.getText().toString()) ||
                isEmpty(height.getText().toString()) ||
                isEmpty(username.getText().toString())) {
            errMsg = "All field must be filled!";
            return 1;
        }
        if (fullname.length() > 30) {
            errMsg = "Full name maximum 30 characters!";
            return 2;
        }
        if (!isValidEmail(email.getText().toString())) {
            errMsg = "Invalid Email!";
            return 3;
        }
        if (password.length() < 8) {
            errMsg = "Password must be 8 characters or more!";
            return 4;
        }
        if (!password.getText().toString().equals(confirmationPassword.getText().toString())) {
            errMsg = "Confirmation password failed!";
            return 5;
        }
        if (username.length() < 8) {
            errMsg = "Username must be 8 characters or more!";
            return 6;
        }
        if (!username.getText().toString().matches("[a-zA-Z0-9]+")) {
//            Toast.makeText(this, "Username must be alphanumeric!", Toast.LENGTH_SHORT).show();
            errMsg = "Username must be alphanumeric!";
            return 7;
        }
        if (tempCheck != 0) {
            if (tempCheck == 1) errMsg = "Email already exist!";
            else if (tempCheck == 2) errMsg = "Username already exist!";
            return 8;
        }
        return 0;
    }

    private void showImagePickerOptions(final int requestCode) {
        ImagePickerActivity.showImagePickerOptions(this, new ImagePickerActivity.PickerOptionListener() {
            @Override
            public void onTakeCameraSelected() {
                launchCameraIntent(requestCode);
            }

            @Override
            public void onChooseGallerySelected() {
                launchGalleryIntent(requestCode);
            }
        });
    }

    private void launchCameraIntent(int requestCode) {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, false);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);

        startActivityForResult(intent, requestCode);
    }

    private void launchGalleryIntent(int requestCode) {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, false);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 200) {
            if (resultCode == Activity.RESULT_OK) {
                uri = data.getParcelableExtra("path");
                ivPhoto.setImageURI(uri);
            }
        }
    }

    private String doUpload(final Uri uris) {
        final String[] photoUrl = new String[1];

        if (mProgressDialog == null) {
            mProgressDialog = ProgressDialog.show(this, "Processing",
                    "Doing Upload...", true);
        } else {
            mProgressDialog.show();
        }

        final StorageReference storageReference = storageRef.child("test/images/" + user.getUsername() + ".jpg");

//        storageReference.putFile(uris).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
//            @Override
//            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
//                if (!task.isSuccessful()){
//
//                    throw task.getException();
//                }
//                Toast.makeText(RegisterActivity.this, "Yey", Toast.LENGTH_SHORT).show();
//
//                return storageReference.getDownloadUrl();
//            }
//        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
//            @Override
//            public void onComplete(@NonNull Task<Uri> task) {
//                Toast.makeText(RegisterActivity.this, "nope", Toast.LENGTH_SHORT).show();
//
//                databaseReference.orderByChild("username").equalTo(user.getUsername()).addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        Toast.makeText(RegisterActivity.this, "ga", Toast.LENGTH_SHORT).show();
//
//                        if (dataSnapshot.exists()) {
//                            Toast.makeText(RegisterActivity.this, "uhu", Toast.LENGTH_SHORT).show();
//
//                            for (DataSnapshot datas : dataSnapshot.getChildren()) {
//                                Toast.makeText(RegisterActivity.this, "ehe", Toast.LENGTH_SHORT).show();
//
//                                final String key = datas.getKey();
//                                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
//                                    @Override
//                                    public void onSuccess(Uri uri) {
//                                        photoUrl[0] = uri.toString();
//                                        user.setPhotoUrl(photoUrl[0]);
//                                        insertUserToDB(user);
////                                        table_buyer.push().setValue(buyer);
//
//                                    }
//                                });
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//
//
//                });
//            }
//        });

        storageReference.putFile(uris).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                storageReference.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        photoUrl[0] = uri.toString();
                        user.setPhotoUrl(photoUrl[0]);
//                        databaseReference.child("users").child(user.getUsername()).setValue(user);
                        insertUserToDB(user);

                    }
                });
//                databaseReference.orderByChild("username").equalTo(user.getUsername()).addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(DataSnapshot dataSnapshot) {
//                        if (dataSnapshot.exists()) {
//                            for (DataSnapshot datas : dataSnapshot.getChildren()) {
//                                final String key = datas.getKey();
//
//                            }
//                        }
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//
//
//                });
                mProgressDialog.dismiss();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.dismiss();
            }
        });
        mProgressDialog.dismiss();
        return photoUrl[0];
    }

}
