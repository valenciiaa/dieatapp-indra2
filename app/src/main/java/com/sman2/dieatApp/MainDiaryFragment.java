package com.sman2.dieatApp;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.sman2.dieatApp.adapter.BreakfastRecylerViewAdapter;
import com.sman2.dieatApp.adapter.DinnerRecyclerViewAdapter;
import com.sman2.dieatApp.adapter.LunchRecyclerViewAdapter;
import com.sman2.dieatApp.adapter.WorkoutRecyclerViewAdapter;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.Food;
import com.sman2.dieatApp.model.Workout;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * A simple {@link Fragment} subclass.
 */
public class MainDiaryFragment extends Fragment {

    public RecyclerView.LayoutManager layoutManager;
    TextView dateDiary, txtTotalBreakfast, txtTotalLunch, txtTotalDinner, txtTotalWorkout;
    FrameLayout breakfastAddBtn, lunchAddBtn, dinnerAddBtn, workoutAddBtn;
    ImageView btnYesterday, btnNextDay;
    Date currentDate;
    FirebaseDatabase database;
    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";
    private DatabaseReference databaseReference;
    private RecyclerView rvWorkout, rvBreakfast, rvLunch, rvDinner;
    private double totalBreakfast = 0,
            totalLunch = 0, totalDinner = 0, totalWorkout = 0;


    public MainDiaryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        totalBreakfast = 0;
        totalLunch = 0;
        totalDinner = 0;
        totalWorkout = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main_diary, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            currentDate = new Date(getArguments().getLong(TAG_TIME));
        } else {
            currentDate = new Date();
        }

        SimpleDateFormat formatter = new SimpleDateFormat("dd MMM yyyy");
        txtTotalBreakfast = view.findViewById(R.id.txtTotalBreakfast);
        txtTotalDinner = view.findViewById(R.id.txtTotalDinner);
        txtTotalLunch = view.findViewById(R.id.txtTotalLunch);
        txtTotalWorkout = view.findViewById(R.id.txtTotalWorkout);
        btnYesterday = view.findViewById(R.id.btnYesterday);
        btnNextDay = view.findViewById(R.id.btnNextDay);
        breakfastAddBtn = view.findViewById(R.id.breakfastAddBtn);
        lunchAddBtn = view.findViewById(R.id.lunchAddBtn);
        dinnerAddBtn = view.findViewById(R.id.dinnerAddBtn);
        workoutAddBtn = view.findViewById(R.id.workoutAddBtn);
        dateDiary = view.findViewById(R.id.dateDiary);
        rvWorkout = view.findViewById(R.id.rvWorkout);
        rvWorkout.setHasFixedSize(false);
        rvWorkout.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvBreakfast = view.findViewById(R.id.rvBreakfast);
        rvBreakfast.setHasFixedSize(false);
        rvBreakfast.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvLunch = view.findViewById(R.id.rvLunch);
        rvLunch.setHasFixedSize(false);
        rvLunch.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvDinner = view.findViewById(R.id.rvDinner);
        rvDinner.setHasFixedSize(false);
        rvDinner.setLayoutManager(new LinearLayoutManager(getActivity()));
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
        database = FirebaseDatabase.getInstance();
        databaseReference = database.getReference("users").child(Session.currentUser.getUsername()).child(sdf.format(currentDate));

        dateDiary.setText(formatter.format(currentDate));

        btnNextDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mainDiaryFragment = new MainDiaryFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime() + 24 * 60 * 60 * 1000);
                mainDiaryFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, mainDiaryFragment).commit();
            }
        });

        btnYesterday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment mainDiaryFragment = new MainDiaryFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime() - 24 * 60 * 60 * 1000);
                mainDiaryFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, mainDiaryFragment).commit();
            }
        });


        breakfastAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addFoodFragment = new AddFoodFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime());
                bundle.putString(TAG_TYPE, "BREAKFAST");
                addFoodFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, addFoodFragment).commit();
                fragmentTransaction.addToBackStack(MainDiaryFragment.class.getSimpleName());
            }
        });

        lunchAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addFoodFragment = new AddFoodFragment();
                Bundle bundle = new Bundle();
                bundle.putString(TAG_TYPE, "LUNCH");
                bundle.putLong(TAG_TIME, currentDate.getTime());
                addFoodFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, addFoodFragment).commit();
                fragmentTransaction.addToBackStack(MainDiaryFragment.class.getSimpleName());
            }
        });

        dinnerAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment addFoodFragment = new AddFoodFragment();
                Bundle bundle = new Bundle();
                bundle.putString(TAG_TYPE, "DINNER");
                bundle.putLong(TAG_TIME, currentDate.getTime());
                addFoodFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, addFoodFragment).commit();
                fragmentTransaction.addToBackStack(MainDiaryFragment.class.getSimpleName());
            }
        });

        workoutAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment searchWorkoutFragment = new SearchWorkoutFragment();
                Bundle bundle = new Bundle();
                bundle.putLong(TAG_TIME, currentDate.getTime());
                searchWorkoutFragment.setArguments(bundle);
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.rootLayout, searchWorkoutFragment).commit();
                fragmentTransaction.addToBackStack(MainDiaryFragment.class.getSimpleName());
            }
        });

        breakfastLoad();
        lunchLoad();
        dinnerLoad();
        workoutLoad();
    }

    private void dinnerLoad() {
        Query query = databaseReference.child("DINNER").orderByChild("workoutName");
        FirebaseRecyclerOptions<Food> options = new FirebaseRecyclerOptions.Builder<Food>().setQuery(query, Food.class).build();
        FirebaseRecyclerAdapter<Food, DinnerRecyclerViewAdapter> adapter = new FirebaseRecyclerAdapter<Food, DinnerRecyclerViewAdapter>(options) {
            @Override
            protected void onBindViewHolder(@NonNull DinnerRecyclerViewAdapter holder, int position, @NonNull Food model) {
                holder.dinnerName.setText(model.getName());
                holder.dinnerDesc.setText(model.getCalories() + " kkal per " + model.getServingSize() + " " + model.getServingUnit());
            }

            @NonNull
            @Override
            public DinnerRecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_dinner_list, parent, false);
                return new DinnerRecyclerViewAdapter(view);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                for (int i = 0; i < getSnapshots().size(); i++) {
                    totalDinner += Double.valueOf(getSnapshots().get(i).getCalories());
                }
                txtTotalDinner.setText(String.format("%.2f", totalDinner));
            }
        };
        adapter.startListening();
        rvDinner.setAdapter(adapter);
    }

    private void lunchLoad() {
        Query query = databaseReference.child("LUNCH").orderByChild("workoutName");
        FirebaseRecyclerOptions<Food> options = new FirebaseRecyclerOptions.Builder<Food>().setQuery(query, Food.class).build();
        FirebaseRecyclerAdapter<Food, LunchRecyclerViewAdapter> adapter = new FirebaseRecyclerAdapter<Food, LunchRecyclerViewAdapter>(options) {
            @Override
            protected void onBindViewHolder(@NonNull LunchRecyclerViewAdapter holder, int position, @NonNull Food model) {
                holder.lunchName.setText(model.getName());
                holder.lunchDesc.setText(model.getCalories() + " kkal per " + model.getServingSize() + " " + model.getServingUnit());
                totalLunch += Double.valueOf(model.getCalories());
            }

            @NonNull
            @Override
            public LunchRecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_lunch_list, parent, false);
                return new LunchRecyclerViewAdapter(view);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                for (int i = 0; i < getSnapshots().size(); i++) {
                    totalLunch += Double.valueOf(getSnapshots().get(i).getCalories());
                }
                txtTotalLunch.setText(String.format("%.2f", totalLunch));

            }
        };
        adapter.startListening();
        rvLunch.setAdapter(adapter);

    }

    private void breakfastLoad() {

        Query query = databaseReference.child("BREAKFAST").orderByChild("workoutName");
        FirebaseRecyclerOptions<Food> options = new FirebaseRecyclerOptions.Builder<Food>().setQuery(query, Food.class).build();
        FirebaseRecyclerAdapter<Food, BreakfastRecylerViewAdapter> adapter = new FirebaseRecyclerAdapter<Food, BreakfastRecylerViewAdapter>(options) {
            @Override
            protected void onBindViewHolder(@NonNull BreakfastRecylerViewAdapter holder, int position, @NonNull Food model) {
                holder.breakfastName.setText(model.getName());
                holder.breakfastDesc.setText(model.getCalories() + " kkal per " + model.getServingSize() + " " + model.getServingUnit());
                totalBreakfast += Double.valueOf(model.getCalories());
            }

            @NonNull
            @Override
            public BreakfastRecylerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_breakfast_list, parent, false);
                return new BreakfastRecylerViewAdapter(view);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                for (int i = 0; i < getSnapshots().size(); i++) {
                    totalBreakfast += Double.valueOf(getSnapshots().get(i).getCalories());
                }
                txtTotalBreakfast.setText(String.format("%.2f", totalBreakfast));
            }
        };
        adapter.startListening();
        rvBreakfast.setAdapter(adapter);

    }

    private void workoutLoad() {
        Query query = databaseReference.child("WORKOUT").orderByChild("workoutName");
        FirebaseRecyclerOptions<Workout> options = new FirebaseRecyclerOptions.Builder<Workout>().setQuery(query, Workout.class).build();
        FirebaseRecyclerAdapter<Workout, WorkoutRecyclerViewAdapter> adapter = new FirebaseRecyclerAdapter<Workout, WorkoutRecyclerViewAdapter>(options) {
            @Override
            protected void onBindViewHolder(@NonNull WorkoutRecyclerViewAdapter holder, int position, @NonNull Workout model) {
                holder.workoutName.setText(model.getWorkoutName());
                holder.workoutCaloriesAndTime.setText(model.getWorkoutCalories() + " kkal, " + model.getWorkoutMinutes() + " minutes");
            }

            @NonNull
            @Override
            public WorkoutRecyclerViewAdapter onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_workout_list_diary, parent, false);
                return new WorkoutRecyclerViewAdapter(view);
            }

            @Override
            public void onDataChanged() {
                super.onDataChanged();
                for (int i = 0; i < getSnapshots().size(); i++) {
                    totalWorkout += Double.valueOf(getSnapshots().get(i).getWorkoutCalories());
                }
                txtTotalWorkout.setText(String.format("%.2f", totalWorkout));

            }
        };
        adapter.startListening();
        rvWorkout.setAdapter(adapter);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
