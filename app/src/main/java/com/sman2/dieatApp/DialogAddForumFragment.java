package com.sman2.dieatApp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.Forum;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogAddForumFragment extends DialogFragment {

    private EditText txtTitle, txtContent;


    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View view = inflater.inflate(R.layout.dialog_add_forum, null);
        txtTitle = view.findViewById(R.id.txtTitle);
        txtContent = view.findViewById(R.id.txtContent);

        builder.setView(view).setTitle("Input forum detail").setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        }).setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doAddForum();
                dialog.dismiss();
            }
        });


        return builder.create();
    }

    private void doAddForum() {
        final String title = txtTitle.getText().toString();
        final String content = txtContent.getText().toString();
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_forum = database.getReference("forums");
        final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Please wait...");
        mDialog.show();
        table_forum.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                Forum newForum = new Forum();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                newForum.setTitle(title);
                newForum.setContent(content);
                newForum.setTotalComments(0);
                newForum.setUserName(Session.currentUser.getUsername());
                newForum.setCreatedAt(sdf.format(new Date()));
                newForum.setForumId(String.valueOf(dataSnapshot.getChildrenCount() + 1));
                table_forum.child(newForum.getForumId()).setValue(newForum);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
