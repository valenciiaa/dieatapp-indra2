package com.sman2.dieatApp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.R;
import com.sman2.dieatApp.model.Comment;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class CommentRecyclerViewAdapter extends RecyclerView.Adapter<CommentRecyclerViewAdapter.ViewHolder> {
    private ArrayList<Comment> listComment;


    public CommentRecyclerViewAdapter(ArrayList<Comment> listComment) {
        this.listComment = listComment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment_list, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Comment comment = listComment.get(position);

        holder.txtCommentContent.setText(comment.getContent());
        holder.txtCommentName.setText(comment.getUserName());
    }

    @Override
    public int getItemCount() {
        return listComment.size();
    }

    private String getTimeString(String pastString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            Date past = format.parse(pastString);
            Date now = new Date();
            long seconds = TimeUnit.MILLISECONDS.toSeconds(now.getTime() - past.getTime());
            long minutes = TimeUnit.MILLISECONDS.toMinutes(now.getTime() - past.getTime());
            long hours = TimeUnit.MILLISECONDS.toHours(now.getTime() - past.getTime());
            long days = TimeUnit.MILLISECONDS.toDays(now.getTime() - past.getTime());

            if (seconds < 60) {
                return (seconds + " seconds ago");
            } else if (minutes < 60) {
                return (minutes + " minutes ago");
            } else if (hours < 24) {
                return (hours + " hours ago");
            } else {
                return (days + " days ago");
            }

        } catch (Exception j) {
            j.printStackTrace();
        }
        return "";
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txtCommentName;
        public TextView txtCommentContent;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCommentContent = itemView.findViewById(R.id.txtCommentContent);
            txtCommentName = itemView.findViewById(R.id.txtCommentName);
        }
    }
}
