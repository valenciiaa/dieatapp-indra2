package com.sman2.dieatApp.model;

import java.io.Serializable;

public class ListWorkoutByUser implements Serializable {

    private String workoutIdListWorkout;
    private String dateListWorkout;
    private int minutesListWorkout;

    public ListWorkoutByUser() {
    }

    public ListWorkoutByUser(String workoutIdListWorkout, String dateListWorkout, int minutesListWorkout) {
        this.workoutIdListWorkout = workoutIdListWorkout;
        this.dateListWorkout = dateListWorkout;
        this.minutesListWorkout = minutesListWorkout;
    }

    public String getWorkoutIdListWorkout() {
        return workoutIdListWorkout;
    }

    public void setWorkoutIdListWorkout(String workoutIdListWorkout) {
        this.workoutIdListWorkout = workoutIdListWorkout;
    }

    public String getDateListWorkout() {
        return dateListWorkout;
    }

    public void setDateListWorkout(String dateListWorkout) {
        this.dateListWorkout = dateListWorkout;
    }

    public int getMinutesListWorkout() {
        return minutesListWorkout;
    }

    public void setMinutesListWorkout(int minutesListWorkout) {
        this.minutesListWorkout = minutesListWorkout;
    }
}
