package com.sman2.dieatApp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SearchView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.adapter.SearchAdapter;
import com.sman2.dieatApp.model.Workout;

import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class SearchWorkoutFragment extends Fragment {

    private static final String TAG_TIME = "TAG_TIME";
    private DatabaseReference databaseReference;

    private SearchView svWorkout;
    private RecyclerView searchRecyclerView;
    private SearchAdapter searchAdapter;
    private ArrayList<Workout> listWorkouts;
    private Date currentDate;
    private Workout workoutClicked;

    public SearchWorkoutFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        System.out.println("0");
        return inflater.inflate(R.layout.fragment_search_workout, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        searchRecyclerView = view.findViewById(R.id.rvActivityList);
        searchRecyclerView.setHasFixedSize(true);
        searchRecyclerView.setLayoutManager(new LinearLayoutManager(SearchWorkoutFragment.super.getContext()));
        if (getArguments() != null) {
            currentDate = new Date(getArguments().getLong(TAG_TIME));
        }
        svWorkout = view.findViewById(R.id.svWorkout);
        listWorkouts = new ArrayList<>();

        databaseReference = FirebaseDatabase.getInstance().getReference();

        databaseReference.child("workouts").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    listWorkouts.add(ds.getValue(Workout.class));
                    searchAdapter = new SearchAdapter(listWorkouts, currentDate, getActivity());

                    svWorkout.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                        @Override
                        public boolean onQueryTextSubmit(String query) {
                            String text = query.toLowerCase(Locale.getDefault());
                            searchAdapter.filter(text);
                            searchRecyclerView.setAdapter(searchAdapter);
                            return false;
                        }

                        @Override
                        public boolean onQueryTextChange(String newText) {
                            return false;
                        }
                    });
                    searchRecyclerView.setAdapter(searchAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
