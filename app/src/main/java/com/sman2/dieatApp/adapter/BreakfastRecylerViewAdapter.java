package com.sman2.dieatApp.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.R;

public class BreakfastRecylerViewAdapter extends RecyclerView.ViewHolder {

    public TextView breakfastName;
    public TextView breakfastDesc;


    public BreakfastRecylerViewAdapter(@NonNull View itemView) {
        super(itemView);
        breakfastName = itemView.findViewById(R.id.txtBreakfastName);
        breakfastDesc = itemView.findViewById(R.id.txtBreakfastDesc);
    }

    void text() {

    }
}
