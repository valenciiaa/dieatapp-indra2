package com.sman2.dieatApp.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Food implements Parcelable {

    private String id;
    private String name;
    private String brandId;
    private String brandName;
    private String calories;
    private String description;
    private String totalFat;
    private String totalProtein;
    private String totalCarbohydrate;
    private String servingSize;
    private String servingUnit;


    public Food(String id, String name, String brandId, String brandName, String calories, String description, String totalFat, String totalProtein, String totalCarbohydrate, String servingSize, String servingUnit) {
        this.id = id;
        this.name = name;
        this.brandId = brandId;
        this.brandName = brandName;
        this.calories = calories;
        this.description = description;
        this.totalFat = totalFat;
        this.totalProtein = totalProtein;
        this.totalCarbohydrate = totalCarbohydrate;
        this.servingSize = servingSize;
        this.servingUnit = servingUnit;
    }

    public Food() {
    }

    public static final Creator<Food> CREATOR = new Creator<Food>() {
        @Override
        public Food createFromParcel(Parcel in) {
            return new Food(in);
        }

        @Override
        public Food[] newArray(int size) {
            return new Food[size];
        }
    };

    protected Food(Parcel in) {
        id = in.readString();
        name = in.readString();
        brandId = in.readString();
        brandName = in.readString();
        calories = in.readString();
        description = in.readString();
        totalFat = in.readString();
        totalProtein = in.readString();
        totalCarbohydrate = in.readString();
        servingSize = in.readString();
        servingUnit = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(brandId);
        dest.writeString(brandName);
        dest.writeString(calories);
        dest.writeString(description);
        dest.writeString(totalFat);
        dest.writeString(totalProtein);
        dest.writeString(totalCarbohydrate);
        dest.writeString(servingSize);
        dest.writeString(servingUnit);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getServingSize() {
        return servingSize;
    }

    public void setServingSize(String servingSize) {
        this.servingSize = servingSize;
    }

    public String getServingUnit() {
        return servingUnit;
    }

    public void setServingUnit(String servingUnit) {
        this.servingUnit = servingUnit;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrandId() {
        return brandId;
    }

    public void setBrandId(String brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getCalories() {
        return calories;
    }

    public void setCalories(String calories) {
        this.calories = calories;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTotalFat() {
        return totalFat;
    }

    public void setTotalFat(String totalFat) {
        this.totalFat = totalFat;
    }

    public String getTotalProtein() {
        return totalProtein;
    }

    public void setTotalProtein(String totalProtein) {
        this.totalProtein = totalProtein;
    }

    public String getTotalCarbohydrate() {
        return totalCarbohydrate;
    }

    public void setTotalCarbohydrate(String totalCarbohydrate) {
        this.totalCarbohydrate = totalCarbohydrate;
    }
}
