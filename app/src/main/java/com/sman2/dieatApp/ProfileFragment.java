package com.sman2.dieatApp;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.sman2.dieatApp.common.Session;

import org.w3c.dom.Text;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    TextView profileUserNameTxt,profileUserGenderTxt, profileUserWeightTxt, profileUserHeightTxt, profileUserEmailTxt;



    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        profileUserNameTxt = view.findViewById(R.id.profileUserNameTxt);
        profileUserGenderTxt = view.findViewById(R.id.profileUserGenderTxt);
        profileUserHeightTxt = view.findViewById(R.id.profileUserHeightTxt);
        profileUserEmailTxt = view.findViewById(R.id.profileUserEmailTxt);
        profileUserWeightTxt = view.findViewById(R.id.profileUserWeightTxt);

        profileUserNameTxt.setText(Session.currentUser.getFullName());
        profileUserGenderTxt.setText(Session.currentUser.getGender());
        profileUserHeightTxt.setText(Session.currentUser.getHeight() + "cm");
        profileUserEmailTxt.setText(Session.currentUser.getEmail());
        profileUserWeightTxt.setText(Session.currentUser.getWeight() + "kg");


    }
}
