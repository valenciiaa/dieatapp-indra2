package com.sman2.dieatApp;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;

import com.sman2.dieatApp.common.Session;


public class SplashActivity extends Activity{
    Handler handler;
    private Session session;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_file);

        handler = new Handler();
        session = new Session(SplashActivity.this);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                session.checkLogin();
                finish();
            }
        }, 3000);


    }


}
