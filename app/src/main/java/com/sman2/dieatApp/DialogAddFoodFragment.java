package com.sman2.dieatApp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.sman2.dieatApp.common.Session;
import com.sman2.dieatApp.model.Food;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DialogAddFoodFragment extends DialogFragment {
    Date currentDate;
    Food food;
    String type;
    String TAG_TYPE = "TAG_TYPE";
    String TAG_TIME = "TAG_TIME";
    String TAG_FOOD = "TAG_FOOD";
    private EditText txtServing;
    private Context context;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();

        if (getArguments() != null) {
            food = getArguments().getParcelable(TAG_FOOD);
            currentDate = new Date(getArguments().getLong(TAG_TIME));
            type = getArguments().getString(TAG_TYPE);
        }

        View view = inflater.inflate(R.layout.dialog_add_food, null);

        builder.setView(view).setTitle("Input the number of serving").setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        }).setPositiveButton("ADD", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                doAddFood();
                dialog.dismiss();
            }
        });
        txtServing = (EditText) view.findViewById(R.id.txtServing);


        return builder.create();
    }

    private void doAddFood() {
        final int totalServing = Integer.valueOf(txtServing.getText().toString());
        SimpleDateFormat sdf = new SimpleDateFormat("ddMMMyyyy");
        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference table_user = database.getReference("users").child(Session.currentUser.getUsername()).child(sdf.format(currentDate)).child(type);
        final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage("Please wait...");
        mDialog.show();
        table_user.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mDialog.dismiss();
                food.setServingSize(String.valueOf(totalServing));
                food.setCalories(String.valueOf(totalServing * Double.valueOf(food.getCalories())));
                food.setTotalFat(String.valueOf(totalServing * Double.valueOf(food.getTotalFat())));
                food.setTotalProtein(String.valueOf(totalServing * Double.valueOf(food.getTotalProtein())));
                food.setTotalCarbohydrate(String.valueOf(totalServing * Double.valueOf(food.getTotalCarbohydrate())));
//                table_user.addListenerForSingleValueEvent(new ValueEventListener() {
//                    @Override
//                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                        workout.setId(String.valueOf(dataSnapshot.getChildrenCount() + 1));
//                    }
//
//                    @Override
//                    public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                    }
//                });
                table_user.push().setValue(food);


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
