package com.sman2.dieatApp.adapter;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.sman2.dieatApp.R;

public class WorkoutRecyclerViewAdapter extends RecyclerView.ViewHolder {


    public TextView workoutName;
    public TextView workoutCaloriesAndTime;
    public CardView activityCardView;


    public WorkoutRecyclerViewAdapter(@NonNull View itemView) {
        super(itemView);
        workoutName = itemView.findViewById(R.id.txtActivityNameDiary);
        workoutCaloriesAndTime = itemView.findViewById(R.id.txtActivityCaloriesAndTimeDiary);
        activityCardView = itemView.findViewById(R.id.activity_card_view_diary);
    }



}
